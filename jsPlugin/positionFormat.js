
function PositionFormat(){

	for (var i = 0; i < groupTotal.children.length; i++) {
		if (i == 8) continue;
		for (var j = 0; j < groupTotal.children[i].children.length; j++) {
			groupTotal.children[i].children[j].castShadow = true;
			if (i == 0)
			groupTotal.children[i].children[j].receiveShadow = true;
		// var material = new THREE.MeshPhongMaterial( {color: color,shininess: shines,specular: specularColor,  emissive: 0x000000, transparent:true, opacity: opacity});//,,	side: THREE.FrontSide
			groupTotal.children[i].children[j].material.shininess = shines;
			// groupTotal.children[i].children[j].material.specular.r = specularColor;
			// groupTotal.children[i].children[j].material.specular.g = specularColor;
			// groupTotal.children[i].children[j].material.specular.b = specularColor;
			// groupTotal.children[i].children[j].material.emissive = emissiveColor;
		}
		if (i == 0) continue;
		groupTotal.children[i].position.set( deltaX[i], 0, totalRadius);
		groupTotal.children[i].scale.x = modelScale[i];
		groupTotal.children[i].scale.y = modelScale[i];
		groupTotal.children[i].scale.z = modelScale[i];
	
		var distance, radius;
				distance = totalRadius + deltaRadius[i];
				radius = (stepAngle[i])/180 * Math.PI;
			groupTotal.children[i].position.y = distance * Math.sin(radius) * -1;
			groupTotal.children[i].position.z = distance * Math.cos(radius);
			groupTotal.children[i].rotation.x = radius;
	}

}

	function DistanceCal(x, z){
		return Math.sqrt(x*x + z*z);
	}

	function AddLight(){
	       var distance, radius, deltaHeight = 30;
	       var buildingHeight = [450, 350, 390, 350, 450, 290];
		for (var i = 0; i < 2; i++) { // left and right
			for (var j = 0; j < 3; j++) { // from outsite
				for (var k = 1; k < 2; k++) { // bottom and top
					 for (var m = 0; m < 2; m++) { // front and center
					 		// deltaHeight = (buildingHeight[i*3+j]/3) * (11/50);
				             distance = totalRadius + deltaHeight * k;
				             radius = (stepAngle[5]+1.2 * m)/180 * Math.PI;
				       var spotFiveLight;
				       if (m == 0)
				       spotFiveLight = new THREE.PointLight( 0x8888ff, 100, 100, 10);
				   		else spotFiveLight = new THREE.SpotLight( 0x8888ff, 80, 80, 10);
				          spotFiveLight.position.y = distance * Math.sin(radius) * -1;
				          spotFiveLight.position.z = distance * Math.cos(radius);
				          if (i == 0)
				          	spotFiveLight.position.x = -310 + 67 * j;
				          else spotFiveLight.position.x = 310 - 67 * j;
				          // spotFiveLight.exponent = 200;
				          spotFiveLight.angle = Math.PI*1.5;
				          spotFiveLight.castShadow = false;
				       groupTotal.add( spotFiveLight );

					 }
				}
			}
		}
	       // var spotThreeLight = new THREE.PointLight( 0x8888FF, 20, 50, 10)//, 50, 1);
	       //       distance = totalRadius + 40;
	       //       radius = (stepAngle[3]+1.7)/180 * Math.PI;
	       //    spotThreeLight.position.y = distance * Math.sin(radius) * -1;
	       //    spotThreeLight.position.z = distance * Math.cos(radius);
	       //    spotThreeLight.position.x = 0;
	       //    // spotThreeLight.exponent = 100;
	       //    // dirLight.castShadow = true;
	       // groupTotal.add( spotThreeLight );

	       var spotThreeLight = new THREE.PointLight( 0x8888FF, 15, 50, 5)//, 50, 1);
	             distance = totalRadius + 0;
	             radius = (stepAngle[3]+0.8)/180 * Math.PI;
	          spotThreeLight.position.y = distance * Math.sin(radius) * -1;
	          spotThreeLight.position.z = distance * Math.cos(radius);
	          spotThreeLight.position.x = 23;
	       groupTotal.add( spotThreeLight );

	       var spotThreeLight = new THREE.PointLight( 0x8888FF, 15, 50, 10)//, 50, 1);
	             distance = totalRadius + 35;
	             radius = (stepAngle[3]-0)/180 * Math.PI;
	          spotThreeLight.position.y = distance * Math.sin(radius) * -1;
	          spotThreeLight.position.z = distance * Math.cos(radius);
	          spotThreeLight.position.x = 0;
	       groupTotal.add( spotThreeLight );

	       var spotThreeLight = new THREE.PointLight( 0x8888FF, 15, 40, 5)//, 50, 1);
	             distance = totalRadius -2;
	             radius = (stepAngle[3]+0.8)/180 * Math.PI;
	          spotThreeLight.position.y = distance * Math.sin(radius) * -1;
	          spotThreeLight.position.z = distance * Math.cos(radius);
	          spotThreeLight.position.x = -23;
	       groupTotal.add( spotThreeLight );

			// rectLight = new THREE.RectAreaLight( 0x0000ff, 500, 10, 10 );//;
			// console.log(rectLight);
			// rectLight.position.set(0, distance * Math.sin(radius) * -1, distance * Math.cos(radius) );

			// 	// TODO: ensure RectAreaLight handles target param correctly

			// rectLightHelper = new THREE.RectAreaLightHelper( rectLight );
			// 	groupTotal.add( rectLightHelper );

	       // var spotTwo1Light = new THREE.SpotLight( 0x0000ff, 50);
	       //       distance = totalRadius + 110;
	       //       radius = (stepAngle[2]+1)/180 * Math.PI;
	       //    spotTwo1Light.position.y = distance * Math.sin(radius) * -1;
	       //    spotTwo1Light.position.z = distance * Math.cos(radius);
	       //    spotTwo1Light.position.x = -177;
	       //    spotTwo1Light.target.position.y = distance * Math.sin(radius) * -1+2;
	       //    spotTwo1Light.target.position.z = distance * Math.cos(radius);
	       //    spotTwo1Light.target.position.x = -182;
	       //    spotTwo1Light.angle = 0.1;
	       //    spotTwo1Light.exponent = 2;
	       //    // dirLight.castShadow = true;
	       // groupTotal.add( spotTwo1Light );

				var Geometry = new THREE.SphereGeometry( 1, 16, 8 );
				twoLight = new THREE.SpotLight( 0x8888ff, 10, 50, Math.PI/3, 0.5, 5 );

				bulbMat = new THREE.MeshBasicMaterial( {
					emissive: 0x8888ff,
					emissiveIntensity: 1,
					color: 0x8888ff
				});
				twoLight.add( new THREE.Mesh( Geometry, bulbMat ) );
				// bulbLight.position.set( 0, 2, 0 );
				// bulbLight.castShadow = true;
				groupTotal.add( twoLight );

	             distance = totalRadius + 100;
	             radius = (stepAngle[2]+0.4)/180 * Math.PI;
	          twoLight.position.y = distance * Math.sin(radius) * -1;
	          twoLight.position.z = distance * Math.cos(radius);
	          twoLight.position.x = -215;

	          twoLight.target.position.y = twoLight.position.y;
	          twoLight.target.position.z = twoLight.position.z;
	          twoLight.target.position.x = twoLight.position.x - 5;
				// hemiLight = new THREE.HemisphereLight( 0xddeeff, 0x0f0e0d, 0.02 );
				// scene.add( hemiLight );

				// floorMat = new THREE.MeshStandardMaterial( {
				// 	roughness: 0.8,
				// 	color: 0xffffff,
				// 	metalness: 0.2,
				// 	bumpScale: 0.0005
				// });
	}

// blueColor = 0xB8E9F6;