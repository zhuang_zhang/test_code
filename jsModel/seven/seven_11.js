function AddSevenModel11(scene)
{
      var mesh;
      var material;
      var geometry;
         geometry = new THREE.BufferGeometry();
         geometry.attributes = {
         position: {
            itemSize: 3,
            array: new Float32Array([
            -12.993, -123.515, 47.161,
            -12.993, -22.731, 47.421,
            -118.720, -22.731, 47.421,
            -12.993, -123.143, 16.683,
            -12.993, -22.943, 16.942,
            -118.720, -22.943, 16.942,
            -12.993, -122.772, -13.793,
            -12.993, -23.158, -13.536,
            -118.720, -23.158, -13.536,
            -12.993, -122.399, -44.273,
            -12.993, -23.373, -44.017,
            -118.720, -23.373, -44.017,
            -12.993, -122.027, -74.750,
            -12.993, -23.587, -74.496,
            -118.720, -23.587, -74.496,
            -12.993, -121.654, -105.229,
            -12.993, -23.800, -104.977,
            -118.720, -23.800, -104.977,
            -118.720, -22.731, 47.421,
            -118.720, -123.515, 47.161,
            -12.993, -123.515, 47.161,
            -12.993, -121.285, -135.706,
            -12.993, -24.018, -135.454,
            -118.720, -24.018, -135.454,
            -118.720, -123.342, 33.065,
            -118.720, -22.829, 33.324,
            -12.993, -22.829, 33.324,
            -12.993, -22.829, 33.324,
            -12.993, -123.342, 33.065,
            -118.720, -123.342, 33.065,
            -118.720, -22.943, 16.942,
            -118.720, -123.143, 16.683,
            -12.993, -123.143, 16.683,
            -12.993, -120.912, -166.185,
            -12.993, -24.229, -165.935,
            -118.720, -24.229, -165.935,
            -118.720, -122.971, 2.587,
            -118.720, -23.043, 2.845,
            -12.993, -23.043, 2.845,
            -12.993, -23.043, 2.845,
            -12.993, -122.971, 2.587,
            -118.720, -122.971, 2.587,
            -118.720, -23.158, -13.536,
            -118.720, -122.772, -13.793,
            -12.993, -122.772, -13.793,
            -12.993, -120.542, -196.663,
            -12.993, -24.445, -196.414,
            -118.720, -24.445, -196.414,
            -12.993, -23.258, -27.633,
            -12.993, -122.600, -27.890,
            -118.720, -122.600, -27.890,
            -118.720, -122.600, -27.890,
            -118.720, -23.258, -27.633,
            -12.993, -23.258, -27.633,
            -118.720, -23.373, -44.017,
            -118.720, -122.399, -44.273,
            -12.993, -122.399, -44.273,
            -118.720, -122.227, -58.369,
            -118.720, -23.473, -58.114,
            -12.993, -23.473, -58.114,
            -12.993, -23.473, -58.114,
            -12.993, -122.227, -58.369,
            -118.720, -122.227, -58.369,
            -118.720, -23.587, -74.496,
            -118.720, -122.027, -74.750,
            -12.993, -122.027, -74.750,
            -118.720, -121.856, -88.846,
            -118.720, -23.687, -88.592,
            -12.993, -23.687, -88.592,
            -12.993, -23.687, -88.592,
            -12.993, -121.856, -88.846,
            -118.720, -121.856, -88.846,
            -118.720, -23.800, -104.977,
            -118.720, -121.654, -105.229,
            -12.993, -121.654, -105.229,
            -118.720, -121.483, -119.325,
            -118.720, -23.899, -119.073,
            -12.993, -23.899, -119.073,
            -12.993, -23.899, -119.073,
            -12.993, -121.483, -119.325,
            -118.720, -121.483, -119.325,
            -118.720, -24.018, -135.454,
            -118.720, -121.285, -135.706,
            -12.993, -121.285, -135.706,
            -118.720, -121.113, -149.802,
            -118.720, -24.115, -149.551,
            -12.993, -24.115, -149.551,
            -12.993, -24.115, -149.551,
            -12.993, -121.113, -149.802,
            -118.720, -121.113, -149.802,
            -118.720, -24.229, -165.935,
            -118.720, -120.912, -166.185,
            -12.993, -120.912, -166.185,
            -118.720, -120.738, -180.281,
            -118.720, -24.328, -180.031,
            -12.993, -24.328, -180.031,
            -12.993, -24.328, -180.031,
            -12.993, -120.738, -180.281,
            -118.720, -120.738, -180.281,
            -118.720, -24.445, -196.414,
            -118.720, -120.542, -196.663,
            -12.993, -120.542, -196.663,
            -118.720, -120.370, -210.759,
            -118.720, -24.545, -210.510,
            -12.993, -24.545, -210.510,
            -12.993, -24.545, -210.510,
            -12.993, -120.370, -210.759,
            -118.720, -120.370, -210.759,
            -118.720, -122.772, -13.793,
            -118.720, -122.600, -27.890,
            -12.993, -122.600, -27.890,
            -12.993, -122.600, -27.890,
            -12.993, -122.772, -13.793,
            -118.720, -122.772, -13.793,
            -118.720, -123.515, 47.161,
            -118.720, -123.342, 33.065,
            -12.993, -123.342, 33.065,
            -118.720, -122.027, -74.750,
            -118.720, -121.856, -88.846,
            -12.993, -121.856, -88.846,
            -118.720, -120.912, -166.185,
            -118.720, -120.738, -180.281,
            -12.993, -120.738, -180.281,
            -12.993, -120.738, -180.281,
            -12.993, -120.912, -166.185,
            -118.720, -120.912, -166.185,
            -12.993, -123.342, 33.065,
            -12.993, -123.515, 47.161,
            -118.720, -123.515, 47.161,
            -12.993, -121.856, -88.846,
            -12.993, -122.027, -74.750,
            -118.720, -122.027, -74.750,
            -118.720, -121.654, -105.229,
            -118.720, -121.483, -119.325,
            -12.993, -121.483, -119.325,
            -118.720, -123.143, 16.683,
            -118.720, -122.971, 2.587,
            -12.993, -122.971, 2.587,
            -12.993, -121.483, -119.325,
            -12.993, -121.654, -105.229,
            -118.720, -121.654, -105.229,
            -12.993, -122.971, 2.587,
            -12.993, -123.143, 16.683,
            -118.720, -123.143, 16.683,
            -118.720, -120.542, -196.663,
            -118.720, -120.370, -210.759,
            -12.993, -120.370, -210.759,
            -118.720, -122.399, -44.273,
            -118.720, -122.227, -58.369,
            -12.993, -122.227, -58.369,
            -118.720, -121.285, -135.706,
            -118.720, -121.113, -149.802,
            -12.993, -121.113, -149.802,
            -12.993, -122.227, -58.369,
            -12.993, -122.399, -44.273,
            -118.720, -122.399, -44.273,
            -12.993, -120.370, -210.759,
            -12.993, -120.542, -196.663,
            -118.720, -120.542, -196.663,
            -12.993, -121.113, -149.802,
            -12.993, -121.285, -135.706,
            -118.720, -121.285, -135.706,
            -118.720, -123.342, 33.065,
            -118.720, -123.515, 47.161,
            -118.720, -22.731, 47.421,
            -12.993, -22.829, 33.324,
            -12.993, -22.731, 47.421,
            -12.993, -123.515, 47.161,
            -118.720, -122.971, 2.587,
            -118.720, -123.143, 16.683,
            -118.720, -22.943, 16.942,
            -12.993, -23.043, 2.845,
            -12.993, -22.943, 16.942,
            -12.993, -123.143, 16.683,
            -118.720, -122.600, -27.890,
            -118.720, -122.772, -13.793,
            -118.720, -23.158, -13.536,
            -12.993, -23.258, -27.633,
            -12.993, -23.158, -13.536,
            -12.993, -122.772, -13.793,
            -12.993, -23.473, -58.114,
            -12.993, -23.373, -44.017,
            -12.993, -122.399, -44.273,
            -118.720, -122.227, -58.369,
            -118.720, -122.399, -44.273,
            -118.720, -23.373, -44.017,
            -12.993, -23.373, -44.017,
            -12.993, -23.473, -58.114,
            -118.720, -23.473, -58.114,
            -118.720, -23.473, -58.114,
            -118.720, -23.373, -44.017,
            -12.993, -23.373, -44.017,
            -12.993, -24.018, -135.454,
            -12.993, -24.115, -149.551,
            -118.720, -24.115, -149.551,
            -118.720, -24.115, -149.551,
            -118.720, -24.018, -135.454,
            -12.993, -24.018, -135.454,
            -118.720, -121.856, -88.846,
            -118.720, -122.027, -74.750,
            -118.720, -23.587, -74.496,
            -12.993, -23.158, -13.536,
            -12.993, -23.258, -27.633,
            -118.720, -23.258, -27.633,
            -12.993, -24.229, -165.935,
            -12.993, -24.328, -180.031,
            -118.720, -24.328, -180.031,
            -12.993, -22.731, 47.421,
            -12.993, -22.829, 33.324,
            -118.720, -22.829, 33.324,
            -12.993, -23.587, -74.496,
            -12.993, -23.687, -88.592,
            -118.720, -23.687, -88.592,
            -12.993, -22.943, 16.942,
            -12.993, -23.043, 2.845,
            -118.720, -23.043, 2.845,
            -12.993, -24.445, -196.414,
            -12.993, -24.545, -210.510,
            -118.720, -24.545, -210.510,
            -118.720, -22.829, 33.324,
            -118.720, -22.731, 47.421,
            -12.993, -22.731, 47.421,
            -118.720, -23.258, -27.633,
            -118.720, -23.158, -13.536,
            -12.993, -23.158, -13.536,
            -118.720, -23.043, 2.845,
            -118.720, -22.943, 16.942,
            -12.993, -22.943, 16.942,
            -118.720, -24.545, -210.510,
            -118.720, -24.445, -196.414,
            -12.993, -24.445, -196.414,
            -118.720, -23.687, -88.592,
            -118.720, -23.587, -74.496,
            -12.993, -23.587, -74.496,
            -118.720, -24.328, -180.031,
            -118.720, -24.229, -165.935,
            -12.993, -24.229, -165.935,
            -12.993, -23.687, -88.592,
            -12.993, -23.587, -74.496,
            -12.993, -122.027, -74.750,
            -12.993, -23.800, -104.977,
            -12.993, -23.899, -119.073,
            -118.720, -23.899, -119.073,
            -118.720, -23.899, -119.073,
            -118.720, -23.800, -104.977,
            -12.993, -23.800, -104.977,
            -118.720, -121.483, -119.325,
            -118.720, -121.654, -105.229,
            -118.720, -23.800, -104.977,
            -12.993, -23.899, -119.073,
            -12.993, -23.800, -104.977,
            -12.993, -121.654, -105.229,
            -12.993, -24.115, -149.551,
            -12.993, -24.018, -135.454,
            -12.993, -121.285, -135.706,
            -118.720, -121.113, -149.802,
            -118.720, -121.285, -135.706,
            -118.720, -24.018, -135.454,
            -118.720, -120.738, -180.281,
            -118.720, -120.912, -166.185,
            -118.720, -24.229, -165.935,
            -12.993, -24.328, -180.031,
            -12.993, -24.229, -165.935,
            -12.993, -120.912, -166.185,
            -118.720, -120.370, -210.759,
            -118.720, -120.542, -196.663,
            -118.720, -24.445, -196.414,
            -12.993, -24.545, -210.510,
            -12.993, -24.445, -196.414,
            -12.993, -120.542, -196.663,
            -12.993, -123.515, 47.161,
            -12.993, -123.342, 33.065,
            -12.993, -22.829, 33.324,
            -118.720, -22.731, 47.421,
            -118.720, -22.829, 33.324,
            -118.720, -123.342, 33.065,
            -12.993, -123.143, 16.683,
            -12.993, -122.971, 2.587,
            -12.993, -23.043, 2.845,
            -118.720, -22.943, 16.942,
            -118.720, -23.043, 2.845,
            -118.720, -122.971, 2.587,
            -12.993, -122.772, -13.793,
            -12.993, -122.600, -27.890,
            -12.993, -23.258, -27.633,
            -118.720, -23.158, -13.536,
            -118.720, -23.258, -27.633,
            -118.720, -122.600, -27.890,
            -118.720, -23.373, -44.017,
            -118.720, -23.473, -58.114,
            -118.720, -122.227, -58.369,
            -12.993, -122.399, -44.273,
            -12.993, -122.227, -58.369,
            -12.993, -23.473, -58.114,
            -12.993, -122.027, -74.750,
            -12.993, -121.856, -88.846,
            -12.993, -23.687, -88.592,
            -118.720, -23.587, -74.496,
            -118.720, -23.687, -88.592,
            -118.720, -121.856, -88.846,
            -12.993, -121.654, -105.229,
            -12.993, -121.483, -119.325,
            -12.993, -23.899, -119.073,
            -118.720, -23.800, -104.977,
            -118.720, -23.899, -119.073,
            -118.720, -121.483, -119.325,
            -118.720, -24.018, -135.454,
            -118.720, -24.115, -149.551,
            -118.720, -121.113, -149.802,
            -12.993, -121.285, -135.706,
            -12.993, -121.113, -149.802,
            -12.993, -24.115, -149.551,
            -12.993, -120.912, -166.185,
            -12.993, -120.738, -180.281,
            -12.993, -24.328, -180.031,
            -118.720, -24.229, -165.935,
            -118.720, -24.328, -180.031,
            -118.720, -120.738, -180.281,
            -12.993, -120.542, -196.663,
            -12.993, -120.370, -210.759,
            -12.993, -24.545, -210.510,
            -118.720, -24.445, -196.414,
            -118.720, -24.545, -210.510,
               -118.720, -120.370, -210.759
            ])
         },
         normal: {
            itemSize: 3,
            array: new Float32Array([
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            -0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            -0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            -0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            -0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            -0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            -0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            -0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            -0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            -0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            -0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            -0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            -0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            -0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            -0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            -0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            -0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            -0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            -0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            1.000, -0.000, 0.000,
            1.000, -0.000, 0.000,
            1.000, -0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            1.000, -0.000, -0.000,
            1.000, -0.000, -0.000,
            1.000, -0.000, -0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            1.000, -0.000, 0.000,
            1.000, -0.000, 0.000,
            1.000, -0.000, 0.000,
            1.000, -0.000, 0.000,
            1.000, -0.000, 0.000,
            1.000, -0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            1.000, -0.000, 0.000,
            1.000, -0.000, 0.000,
            1.000, -0.000, 0.000,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            1.000, -0.000, 0.000,
            1.000, -0.000, 0.000,
            1.000, -0.000, 0.000,
            1.000, -0.000, 0.000,
            1.000, -0.000, 0.000,
            1.000, -0.000, 0.000,
            -1.000, 0.000, -0.000,
            -1.000, 0.000, -0.000,
            -1.000, 0.000, -0.000,
            -1.000, -0.000, 0.000,
            -1.000, -0.000, 0.000,
            -1.000, -0.000, 0.000,
            1.000, -0.000, 0.000,
            1.000, -0.000, 0.000,
            1.000, -0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            1.000, -0.000, -0.000,
            1.000, -0.000, -0.000,
            1.000, -0.000, -0.000,
            1.000, -0.000, -0.000,
            1.000, -0.000, -0.000,
            1.000, -0.000, -0.000,
            -1.000, 0.000, -0.000,
            -1.000, 0.000, -0.000,
            -1.000, 0.000, -0.000,
            1.000, -0.000, -0.000,
            1.000, -0.000, -0.000,
            1.000, -0.000, -0.000,
            -1.000, 0.000, 0.000,
            -1.000, -0.000, 0.000,
            -1.000, 0.000, 0.000,
            1.000, -0.000, -0.000,
            1.000, -0.000, -0.000,
            1.000, -0.000, -0.000,
            -1.000, 0.000, 0.000,
            -1.000, -0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, -0.000, 0.000,
            -1.000, 0.000, 0.000,
            1.000, -0.000, -0.000,
            1.000, -0.000, -0.000,
            1.000, -0.000, -0.000,
            1.000, -0.000, -0.000,
            1.000, -0.000, -0.000,
            1.000, -0.000, -0.000,
            -1.000, 0.000, 0.000,
            -1.000, -0.000, 0.000,
            -1.000, 0.000, 0.000,
            1.000, -0.000, -0.000,
            1.000, -0.000, -0.000,
            1.000, -0.000, -0.000,
            -1.000, 0.000, -0.000,
            -1.000, 0.000, -0.000,
            -1.000, 0.000, -0.000,
            -1.000, 0.000, -0.000,
            -1.000, 0.000, -0.000,
            -1.000, 0.000, -0.000,
            1.000, -0.000, 0.000,
            1.000, -0.000, 0.000,
            1.000, -0.000, 0.000,
            1.000, -0.000, -0.000,
            1.000, -0.000, -0.000,
            1.000, -0.000, -0.000,
            -1.000, 0.000, -0.000,
            -1.000, 0.000, -0.000,
            -1.000, 0.000, -0.000,
            1.000, -0.000, -0.000,
            1.000, -0.000, -0.000,
            1.000, -0.000, -0.000,
            -1.000, 0.000, -0.000,
            -1.000, 0.000, -0.000,
               -1.000, 0.000, -0.000
            ])
         }
         };

         material = new THREE.MeshPhongMaterial({
            color: 0xB8E9F6,
            shininess: 32.000,
            ambient: 0xB8E9F6,
            transparent: true,
            opacity: 0.870,
            side: THREE.FrontSide,
            specular: 0x333333
            });
         mesh = new THREE.Mesh(geometry, material);
         groupModel7.add(mesh);

}
