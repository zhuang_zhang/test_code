function AddSevenModel9(scene)
{
      var mesh;
      var material;
      var geometry;
         geometry = new THREE.BufferGeometry();
         geometry.attributes = {
         position: {
            itemSize: 3,
            array: new Float32Array([
            254.012, -120.626, -195.114,
            254.012, -120.797, -181.017,
            148.285, -120.797, -181.017,
            148.285, -120.797, -181.017,
            148.285, -120.626, -195.114,
            254.012, -120.626, -195.114,
            254.012, -24.395, -194.865,
            254.012, -24.297, -180.767,
            254.012, -120.797, -181.017,
            254.012, -120.797, -181.017,
            254.012, -120.626, -195.114,
            254.012, -24.395, -194.865,
            148.285, -24.395, -194.865,
            148.285, -24.297, -180.767,
            254.012, -24.297, -180.767,
            254.012, -24.297, -180.767,
            254.012, -24.395, -194.865,
            148.285, -24.395, -194.865,
            148.285, -120.626, -195.114,
            148.285, -120.797, -181.017,
            148.285, -24.297, -180.767,
            148.285, -24.297, -180.767,
            148.285, -24.395, -194.865,
            148.285, -120.626, -195.114,
            254.012, -120.797, -181.017,
            254.012, -24.297, -180.767,
            148.285, -24.297, -180.767,
            148.285, -24.297, -180.767,
            148.285, -120.797, -181.017,
            254.012, -120.797, -181.017,
            254.012, -24.395, -194.865,
            254.012, -120.626, -195.114,
            148.285, -120.626, -195.114,
            148.285, -120.626, -195.114,
            148.285, -24.395, -194.865,
            254.012, -24.395, -194.865,
            254.012, -120.998, -164.634,
            254.012, -121.169, -150.538,
            148.285, -121.169, -150.538,
            148.285, -121.169, -150.538,
            148.285, -120.998, -164.634,
            254.012, -120.998, -164.634,
            254.012, -24.182, -164.384,
            254.012, -24.083, -150.286,
            254.012, -121.169, -150.538,
            254.012, -121.169, -150.538,
            254.012, -120.998, -164.634,
            254.012, -24.182, -164.384,
            148.285, -24.182, -164.384,
            148.285, -24.083, -150.286,
            254.012, -24.083, -150.286,
            254.012, -24.083, -150.286,
            254.012, -24.182, -164.384,
            148.285, -24.182, -164.384,
            148.285, -120.998, -164.634,
            148.285, -121.169, -150.538,
            148.285, -24.083, -150.286,
            148.285, -24.083, -150.286,
            148.285, -24.182, -164.384,
            148.285, -120.998, -164.634,
            254.012, -121.169, -150.538,
            254.012, -24.083, -150.286,
            148.285, -24.083, -150.286,
            148.285, -24.083, -150.286,
            148.285, -121.169, -150.538,
            254.012, -121.169, -150.538,
            254.012, -24.182, -164.384,
            254.012, -120.998, -164.634,
            148.285, -120.998, -164.634,
            148.285, -120.998, -164.634,
            148.285, -24.182, -164.384,
            254.012, -24.182, -164.384,
            254.012, -121.369, -134.157,
            254.012, -121.541, -120.061,
            148.285, -121.541, -120.061,
            148.285, -121.541, -120.061,
            148.285, -121.369, -134.157,
            254.012, -121.369, -134.157,
            254.012, -23.967, -133.907,
            254.012, -23.868, -119.808,
            254.012, -121.541, -120.061,
            254.012, -121.541, -120.061,
            254.012, -121.369, -134.157,
            254.012, -23.967, -133.907,
            148.285, -23.967, -133.907,
            148.285, -23.868, -119.808,
            254.012, -23.868, -119.808,
            254.012, -23.868, -119.808,
            254.012, -23.967, -133.907,
            148.285, -23.967, -133.907,
            148.285, -121.369, -134.157,
            148.285, -121.541, -120.061,
            148.285, -23.868, -119.808,
            148.285, -23.868, -119.808,
            148.285, -23.967, -133.907,
            148.285, -121.369, -134.157,
            254.012, -121.541, -120.061,
            254.012, -23.868, -119.808,
            148.285, -23.868, -119.808,
            148.285, -23.868, -119.808,
            148.285, -121.541, -120.061,
            254.012, -121.541, -120.061,
            254.012, -23.967, -133.907,
            254.012, -121.369, -134.157,
            148.285, -121.369, -134.157,
            148.285, -121.369, -134.157,
            148.285, -23.967, -133.907,
            254.012, -23.967, -133.907,
            254.012, -121.739, -103.680,
            254.012, -121.913, -89.584,
            148.285, -121.913, -89.584,
            148.285, -121.913, -89.584,
            148.285, -121.739, -103.680,
            254.012, -121.739, -103.680,
            254.012, -23.751, -103.426,
            254.012, -23.653, -89.330,
            254.012, -121.913, -89.584,
            254.012, -121.913, -89.584,
            254.012, -121.739, -103.680,
            254.012, -23.751, -103.426,
            148.285, -23.751, -103.426,
            148.285, -23.653, -89.330,
            254.012, -23.653, -89.330,
            254.012, -23.653, -89.330,
            254.012, -23.751, -103.426,
            148.285, -23.751, -103.426,
            148.285, -121.739, -103.680,
            148.285, -121.913, -89.584,
            148.285, -23.653, -89.330,
            148.285, -23.653, -89.330,
            148.285, -23.751, -103.426,
            148.285, -121.739, -103.680,
            254.012, -121.913, -89.584,
            254.012, -23.653, -89.330,
            148.285, -23.653, -89.330,
            148.285, -23.653, -89.330,
            148.285, -121.913, -89.584,
            254.012, -121.913, -89.584,
            254.012, -23.751, -103.426,
            254.012, -121.739, -103.680,
            148.285, -121.739, -103.680,
            148.285, -121.739, -103.680,
            148.285, -23.751, -103.426,
            254.012, -23.751, -103.426,
            254.012, -122.113, -73.201,
            254.012, -122.284, -59.106,
            148.285, -122.284, -59.106,
            148.285, -122.284, -59.106,
            148.285, -122.113, -73.201,
            254.012, -122.113, -73.201,
            254.012, -23.538, -72.947,
            254.012, -23.439, -58.850,
            254.012, -122.284, -59.106,
            254.012, -122.284, -59.106,
            254.012, -122.113, -73.201,
            254.012, -23.538, -72.947,
            148.285, -23.538, -72.947,
            148.285, -23.439, -58.850,
            254.012, -23.439, -58.850,
            254.012, -23.439, -58.850,
            254.012, -23.538, -72.947,
            148.285, -23.538, -72.947,
            148.285, -122.113, -73.201,
            148.285, -122.284, -59.106,
            148.285, -23.439, -58.850,
            148.285, -23.439, -58.850,
            148.285, -23.538, -72.947,
            148.285, -122.113, -73.201,
            254.012, -122.284, -59.106,
            254.012, -23.439, -58.850,
            148.285, -23.439, -58.850,
            148.285, -23.439, -58.850,
            148.285, -122.284, -59.106,
            254.012, -122.284, -59.106,
            254.012, -23.538, -72.947,
            254.012, -122.113, -73.201,
            148.285, -122.113, -73.201,
            148.285, -122.113, -73.201,
            148.285, -23.538, -72.947,
            254.012, -23.538, -72.947,
            254.012, -122.485, -42.723,
            254.012, -122.658, -28.626,
            148.285, -122.658, -28.626,
            148.285, -122.658, -28.626,
            148.285, -122.485, -42.723,
            254.012, -122.485, -42.723,
            254.012, -23.325, -42.466,
            254.012, -23.227, -28.368,
            254.012, -122.658, -28.626,
            254.012, -122.658, -28.626,
            254.012, -122.485, -42.723,
            254.012, -23.325, -42.466,
            148.285, -23.325, -42.466,
            148.285, -23.227, -28.368,
            254.012, -23.227, -28.368,
            254.012, -23.227, -28.368,
            254.012, -23.325, -42.466,
            148.285, -23.325, -42.466,
            148.285, -122.485, -42.723,
            148.285, -122.658, -28.626,
            148.285, -23.227, -28.368,
            148.285, -23.227, -28.368,
            148.285, -23.325, -42.466,
            148.285, -122.485, -42.723,
            254.012, -122.658, -28.626,
            254.012, -23.227, -28.368,
            148.285, -23.227, -28.368,
            148.285, -23.227, -28.368,
            148.285, -122.658, -28.626,
            254.012, -122.658, -28.626,
            254.012, -23.325, -42.466,
            254.012, -122.485, -42.723,
            148.285, -122.485, -42.723,
            148.285, -122.485, -42.723,
            148.285, -23.325, -42.466,
            254.012, -23.325, -42.466,
            254.012, -122.856, -12.246,
            254.012, -123.029, 1.849,
            148.285, -123.029, 1.849,
            148.285, -123.029, 1.849,
            148.285, -122.856, -12.246,
            254.012, -122.856, -12.246,
            254.012, -23.110, -11.989,
            254.012, -23.012, 2.108,
            254.012, -123.029, 1.849,
            254.012, -123.029, 1.849,
            254.012, -122.856, -12.246,
            254.012, -23.110, -11.989,
            148.285, -23.110, -11.989,
            148.285, -23.012, 2.108,
            254.012, -23.012, 2.108,
            254.012, -23.012, 2.108,
            254.012, -23.110, -11.989,
            148.285, -23.110, -11.989,
            148.285, -122.856, -12.246,
            148.285, -123.029, 1.849,
            148.285, -23.012, 2.108,
            148.285, -23.012, 2.108,
            148.285, -23.110, -11.989,
            148.285, -122.856, -12.246,
            254.012, -123.029, 1.849,
            254.012, -23.012, 2.108,
            148.285, -23.012, 2.108,
            148.285, -23.012, 2.108,
            148.285, -123.029, 1.849,
            254.012, -123.029, 1.849,
            254.012, -23.110, -11.989,
            254.012, -122.856, -12.246,
            148.285, -122.856, -12.246,
            148.285, -122.856, -12.246,
            148.285, -23.110, -11.989,
            254.012, -23.110, -11.989,
            254.012, -123.227, 18.230,
            254.012, -123.399, 32.327,
            148.285, -123.399, 32.327,
            148.285, -123.399, 32.327,
            148.285, -123.227, 18.230,
            254.012, -123.227, 18.230,
            254.012, -22.895, 18.489,
            254.012, -22.795, 32.587,
            254.012, -123.399, 32.327,
            254.012, -123.399, 32.327,
            254.012, -123.227, 18.230,
            254.012, -22.895, 18.489,
            148.285, -22.895, 18.489,
            148.285, -22.795, 32.587,
            254.012, -22.795, 32.587,
            254.012, -22.795, 32.587,
            254.012, -22.895, 18.489,
            148.285, -22.895, 18.489,
            148.285, -123.227, 18.230,
            148.285, -123.399, 32.327,
            148.285, -22.795, 32.587,
            148.285, -22.795, 32.587,
            148.285, -22.895, 18.489,
            148.285, -123.227, 18.230,
            254.012, -123.399, 32.327,
            254.012, -22.795, 32.587,
            148.285, -22.795, 32.587,
            148.285, -22.795, 32.587,
            148.285, -123.399, 32.327,
            254.012, -123.399, 32.327,
            254.012, -22.895, 18.489,
            254.012, -123.227, 18.230,
            148.285, -123.227, 18.230,
            148.285, -123.227, 18.230,
            148.285, -22.895, 18.489,
            254.012, -22.895, 18.489,
            254.012, -123.600, 48.708,
            254.012, -123.772, 62.804,
            148.285, -123.772, 62.804,
            148.285, -123.772, 62.804,
            148.285, -123.600, 48.708,
            254.012, -123.600, 48.708,
            254.012, -22.682, 48.969,
            254.012, -22.582, 63.066,
            254.012, -123.772, 62.804,
            254.012, -123.772, 62.804,
            254.012, -123.600, 48.708,
            254.012, -22.682, 48.969,
            148.285, -22.682, 48.969,
            148.285, -22.582, 63.066,
            254.012, -22.582, 63.066,
            254.012, -22.582, 63.066,
            254.012, -22.682, 48.969,
            148.285, -22.682, 48.969,
            148.285, -123.600, 48.708,
            148.285, -123.772, 62.804,
            148.285, -22.582, 63.066,
            148.285, -22.582, 63.066,
            148.285, -22.682, 48.969,
            148.285, -123.600, 48.708,
            254.012, -123.772, 62.804,
            254.012, -22.582, 63.066,
            148.285, -22.582, 63.066,
            148.285, -22.582, 63.066,
            148.285, -123.772, 62.804,
            254.012, -123.772, 62.804,
            254.012, -22.682, 48.969,
            254.012, -123.600, 48.708,
            148.285, -123.600, 48.708,
            148.285, -123.600, 48.708,
            148.285, -22.682, 48.969,
               254.012, -22.682, 48.969
            ])
         },
         normal: {
            itemSize: 3,
            array: new Float32Array([
            0.000, -1.000, -0.012,
            -0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            -0.000, -1.000, -0.012,
            1.000, 0.000, -0.000,
            1.000, 0.000, 0.000,
            1.000, -0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, -0.000,
            1.000, 0.000, 0.000,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, -0.000,
            -1.000, 0.000, -0.000,
            -1.000, 0.000, -0.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, -1.000, -0.012,
            -0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            -0.000, -1.000, -0.012,
            1.000, 0.000, -0.000,
            1.000, 0.000, 0.000,
            1.000, -0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, -0.000,
            1.000, 0.000, 0.000,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, -0.000,
            -1.000, 0.000, -0.000,
            -1.000, 0.000, -0.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, -1.000, -0.012,
            -0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            -0.000, -1.000, -0.012,
            1.000, 0.000, -0.000,
            1.000, 0.000, 0.000,
            1.000, -0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, -0.000,
            1.000, 0.000, 0.000,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, -0.000, 0.000,
            -1.000, -0.000, 0.000,
            -1.000, -0.000, 0.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, -1.000, -0.012,
            -0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            -0.000, -1.000, -0.012,
            1.000, 0.000, -0.000,
            1.000, 0.000, 0.000,
            1.000, -0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, -0.000,
            1.000, 0.000, 0.000,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, -0.000,
            -1.000, 0.000, -0.000,
            -1.000, 0.000, -0.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, -1.000, -0.012,
            -0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            -0.000, -1.000, -0.012,
            1.000, 0.000, -0.000,
            1.000, 0.000, 0.000,
            1.000, -0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, -0.000,
            1.000, 0.000, 0.000,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, -0.000, 0.000,
            -1.000, -0.000, 0.000,
            -1.000, -0.000, 0.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, -1.000, -0.012,
            -0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            -0.000, -1.000, -0.012,
            1.000, 0.000, -0.000,
            1.000, 0.000, 0.000,
            1.000, -0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, -0.000,
            1.000, 0.000, 0.000,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            -1.000, -0.000, 0.000,
            -1.000, -0.000, 0.000,
            -1.000, -0.000, 0.000,
            -1.000, 0.000, -0.000,
            -1.000, 0.000, -0.000,
            -1.000, 0.000, -0.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, -1.000, -0.012,
            -0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            -0.000, -1.000, -0.012,
            1.000, 0.000, -0.000,
            1.000, 0.000, 0.000,
            1.000, -0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, -0.000,
            1.000, 0.000, 0.000,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            -1.000, 0.000, -0.000,
            -1.000, 0.000, -0.000,
            -1.000, 0.000, -0.000,
            -1.000, 0.000, 0.000,
            -1.000, -0.000, 0.000,
            -1.000, 0.000, 0.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, -1.000, -0.012,
            -0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            -0.000, -1.000, -0.012,
            1.000, 0.000, -0.000,
            1.000, 0.000, 0.000,
            1.000, -0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, -0.000,
            1.000, 0.000, 0.000,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, -0.000,
            -1.000, 0.000, -0.000,
            -1.000, 0.000, -0.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, -1.000, -0.012,
            -0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            0.000, -1.000, -0.012,
            -0.000, -1.000, -0.012,
            1.000, 0.000, -0.000,
            1.000, 0.000, 0.000,
            1.000, -0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, -0.000,
            1.000, 0.000, 0.000,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            0.000, 1.000, -0.007,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, -0.000, 0.000,
            -1.000, 0.000, 0.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, -0.003, 1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
            0.000, 0.003, -1.000,
               0.000, 0.003, -1.000
            ])
         }
         };

         material = new THREE.MeshPhongMaterial({
            color: 0xB8E9F6,
            shininess: 32.000,
            ambient: 0xB8E9F6,
            transparent: true,
            opacity: 0.870,
            side: THREE.FrontSide,
            specular: 0x333333
            });
         mesh = new THREE.Mesh(geometry, material);
         groupModel7.add(mesh);

}
