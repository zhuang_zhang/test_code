function AddThreeModel2(scene)
{
      var mesh;
      var material;
      var geometry;
         geometry = new THREE.BufferGeometry();
         geometry.attributes = {
         position: {
            itemSize: 3,
            array: new Float32Array([
            -35.355, -45.000, 3.236,
            -0.000, -9.645, 3.236,
            35.355, -45.000, 3.236,
            35.355, -45.000, 3.236,
            0.000, -80.355, 3.236,
            -35.355, -45.000, 3.236,
            -35.355, -45.000, 53.236,
            0.000, -80.355, 53.236,
            35.355, -45.000, 53.236,
            35.355, -45.000, 53.236,
            -0.000, -9.645, 53.236,
            -35.355, -45.000, 53.236,
            -35.355, -45.000, 3.236,
            0.000, -80.355, 3.236,
            0.000, -80.355, 53.236,
            0.000, -80.355, 53.236,
            -35.355, -45.000, 53.236,
            -35.355, -45.000, 3.236,
            0.000, -80.355, 3.236,
            35.355, -45.000, 3.236,
            35.355, -45.000, 53.236,
            35.355, -45.000, 53.236,
            0.000, -80.355, 53.236,
            0.000, -80.355, 3.236,
            35.355, -45.000, 3.236,
            -0.000, -9.645, 3.236,
            -0.000, -9.645, 53.236,
            -0.000, -9.645, 53.236,
            35.355, -45.000, 53.236,
            35.355, -45.000, 3.236,
            -0.000, -9.645, 3.236,
            -35.355, -45.000, 3.236,
            -35.355, -45.000, 53.236,
            -35.355, -45.000, 53.236,
            -0.000, -9.645, 53.236,
               -0.000, -9.645, 3.236
            ])
         },
         normal: {
            itemSize: 3,
            array: new Float32Array([
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            -0.707, -0.707, 0.000,
            -0.707, -0.707, 0.000,
            -0.707, -0.707, 0.000,
            -0.707, -0.707, 0.000,
            -0.707, -0.707, 0.000,
            -0.707, -0.707, 0.000,
            0.707, -0.707, 0.000,
            0.707, -0.707, 0.000,
            0.707, -0.707, 0.000,
            0.707, -0.707, 0.000,
            0.707, -0.707, 0.000,
            0.707, -0.707, 0.000,
            0.707, 0.707, 0.000,
            0.707, 0.707, 0.000,
            0.707, 0.707, 0.000,
            0.707, 0.707, 0.000,
            0.707, 0.707, 0.000,
            0.707, 0.707, 0.000,
            -0.707, 0.707, 0.000,
            -0.707, 0.707, 0.000,
            -0.707, 0.707, 0.000,
            -0.707, 0.707, 0.000,
            -0.707, 0.707, 0.000,
               -0.707, 0.707, 0.000
            ])
         }
         };

         material = new THREE.MeshPhongMaterial({
            color: 0x4580F6,
            shininess: 2.000,
            ambient: 0x4580F6,
            transparent: true,
            opacity: 0.500,
            side: THREE.FrontSide,
            specular: 0xE5E5E5
            });
         mesh = new THREE.Mesh(geometry, material);

              //   // var bulbGeometry = new THREE.SphereGeometry( 1, 16, 8 );
              //   var bulbLight = new THREE.PointLight( 0x0000ff, 500, 50, 1 );

              //   bulbLight.add( mesh );
              //   // var totalRadius = 2060;
              //   // var stepAngle = 60;
              //   console.log(totalRadius+", "+stepAngle[3]);
              //    distance = totalRadius + 50;
              //    radius = (stepAngle[3])/180 * Math.PI;
              // bulbLight.position.y = distance * Math.sin(radius) * -1;
              // bulbLight.position.z = distance * Math.cos(radius);
              // bulbLight.position.x = 0;
              // bulbLight.name = "light1";

              // // bulbLight.target.position.y = distance * Math.sin(radius) * -1;
              // // bulbLight.target.position.z = distance * Math.cos(radius);
              // // bulbLight.target.position.x = -210;
              //   groupTotal.add( bulbLight );



         groupModel3.add(mesh);

}
