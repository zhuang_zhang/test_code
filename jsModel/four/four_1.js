function AddFourModel1(scene)
{
      var mesh;
      var material;
      var geometry;
         geometry = new THREE.BufferGeometry();
         geometry.attributes = {
         position: {
            itemSize: 3,
            array: new Float32Array([
            -233.850, -0.071, 54.815,
            -233.850, -0.060, 59.799,
            -267.540, -32.574, 59.618,
            -267.540, -32.574, 59.618,
            -267.540, -32.507, 54.634,
            -233.850, -0.071, 54.815,
            -267.540, 32.364, 54.491,
            -267.540, 32.452, 59.475,
            -233.850, -0.060, 59.799,
            -233.850, -0.060, 59.799,
            -233.850, -0.071, 54.815,
            -267.540, 32.364, 54.491,
            -301.231, -0.071, 54.815,
            -301.231, -0.060, 59.799,
            -267.540, 32.452, 59.475,
            -267.540, 32.452, 59.475,
            -267.540, 32.364, 54.491,
            -301.231, -0.071, 54.815,
            -267.540, -32.507, 54.634,
            -267.540, -32.574, 59.618,
            -301.231, -0.060, 59.799,
            -301.231, -0.060, 59.799,
            -301.231, -0.071, 54.815,
            -267.540, -32.507, 54.634,
            -233.850, -0.060, 59.799,
            -267.540, 32.452, 59.475,
            -301.231, -0.060, 59.799,
            -301.231, -0.060, 59.799,
            -267.540, -32.574, 59.618,
            -233.850, -0.060, 59.799,
            -267.540, 32.364, 54.491,
            -233.850, -0.071, 54.815,
            -267.540, -32.507, 54.634,
            -267.540, -32.507, 54.634,
            -301.231, -0.071, 54.815,
            -267.540, 32.364, 54.491,
            -222.577, -0.128, 28.898,
            -222.577, -0.085, 48.348,
            -267.540, -43.239, 47.995,
            -267.540, -43.239, 47.995,
            -267.540, -42.878, 28.549,
            -222.577, -0.128, 28.898,
            -267.540, 42.619, 28.360,
            -267.540, 43.066, 47.805,
            -222.577, -0.085, 48.348,
            -222.577, -0.085, 48.348,
            -222.577, -0.128, 28.898,
            -267.540, 42.619, 28.360,
            -312.503, -0.128, 28.898,
            -312.503, -0.085, 48.348,
            -267.540, 43.066, 47.805,
            -267.540, 43.066, 47.805,
            -267.540, 42.619, 28.360,
            -312.503, -0.128, 28.898,
            -267.540, -42.878, 28.549,
            -267.540, -43.239, 47.995,
            -312.503, -0.085, 48.348,
            -312.503, -0.085, 48.348,
            -312.503, -0.128, 28.898,
            -267.540, -42.878, 28.549,
            -222.577, -0.085, 48.348,
            -267.540, 43.066, 47.805,
            -312.503, -0.085, 48.348,
            -312.503, -0.085, 48.348,
            -267.540, -43.239, 47.995,
            -222.577, -0.085, 48.348,
            -267.540, 42.619, 28.360,
            -222.577, -0.128, 28.898,
            -267.540, -42.878, 28.549,
            -267.540, -42.878, 28.549,
            -312.503, -0.128, 28.898,
            -267.540, 42.619, 28.360,
            -229.335, -0.251, -26.577,
            -229.335, -0.137, 25.158,
            -267.540, -36.396, 24.919,
            -267.540, -36.396, 24.919,
            -267.540, -35.597, -26.811,
            -229.335, -0.251, -26.577,
            -267.540, 35.094, -26.967,
            -267.540, 36.121, 24.758,
            -229.335, -0.137, 25.158,
            -229.335, -0.137, 25.158,
            -229.335, -0.251, -26.577,
            -267.540, 35.094, -26.967,
            -305.745, -0.251, -26.577,
            -305.745, -0.137, 25.158,
            -267.540, 36.121, 24.758,
            -267.540, 36.121, 24.758,
            -267.540, 35.094, -26.967,
            -305.745, -0.251, -26.577,
            -267.540, -35.597, -26.811,
            -267.540, -36.396, 24.919,
            -305.745, -0.137, 25.158,
            -305.745, -0.137, 25.158,
            -305.745, -0.251, -26.577,
            -267.540, -35.597, -26.811,
            -229.335, -0.137, 25.158,
            -267.540, 36.121, 24.758,
            -305.745, -0.137, 25.158,
            -305.745, -0.137, 25.158,
            -267.540, -36.396, 24.919,
            -229.335, -0.137, 25.158,
            -267.540, 35.094, -26.967,
            -229.335, -0.251, -26.577,
            -267.540, -35.597, -26.811,
            -267.540, -35.597, -26.811,
            -305.745, -0.251, -26.577,
            -267.540, 35.094, -26.967,
            -89.180, -36.174, 10.588,
            -127.385, -0.168, 10.826,
            -89.180, 35.837, 10.429,
            -50.975, -0.091, 45.626,
            -89.180, 36.527, 45.222,
            -127.385, -0.091, 45.626,
            -89.180, 35.837, 10.429,
            -50.975, -0.168, 10.826,
            -89.180, -36.174, 10.588,
            -127.385, -0.091, 45.626,
            -89.180, -36.711, 45.384,
            -50.975, -0.091, 45.626,
            -89.180, -29.814, -28.487,
            -121.158, -0.254, -28.333,
            -89.180, 29.304, -28.617,
            -57.202, -0.180, 5.500,
            -89.180, 29.878, 5.211,
            -121.158, -0.180, 5.500,
            -89.180, 29.304, -28.617,
            -57.202, -0.255, -28.333,
            -89.180, -29.814, -28.487,
            -121.158, -0.180, 5.500,
            -89.180, -30.239, 5.344,
            -57.202, -0.180, 5.500,
            -89.180, 25.331, 48.945,
            -62.711, -0.083, 49.156,
            -89.180, -25.498, 49.057,
            -62.711, -0.063, 58.553,
            -89.180, 25.466, 58.340,
            -115.649, -0.063, 58.553,
            -89.180, -25.498, 49.057,
            -115.649, -0.083, 49.156,
            -89.180, 25.331, 48.945,
            -115.649, -0.063, 58.553,
            -89.180, -25.592, 58.453,
            -62.711, -0.063, 58.553,
            -50.975, -0.168, 10.826,
            -50.975, -0.091, 45.626,
            -89.180, -36.711, 45.384,
            -50.975, -0.091, 45.626,
            -50.975, -0.168, 10.826,
            -89.180, 35.837, 10.429,
            -127.385, -0.168, 10.826,
            -127.385, -0.091, 45.626,
            -89.180, 36.527, 45.222,
            -127.385, -0.091, 45.626,
            -127.385, -0.168, 10.826,
            -89.180, -36.174, 10.588,
            -89.180, 36.527, 45.222,
            -89.180, 35.837, 10.429,
            -127.385, -0.168, 10.826,
            -89.180, 35.837, 10.429,
            -89.180, 36.527, 45.222,
            -50.975, -0.091, 45.626,
            -89.180, -36.174, 10.588,
            -89.180, -36.711, 45.384,
            -127.385, -0.091, 45.626,
            -89.180, -36.711, 45.384,
            -89.180, -36.174, 10.588,
            -50.975, -0.168, 10.826,
            -57.202, -0.255, -28.333,
            -57.202, -0.180, 5.500,
            -89.180, -30.239, 5.344,
            -57.202, -0.180, 5.500,
            -57.202, -0.255, -28.333,
            -89.180, 29.304, -28.617,
            -121.158, -0.180, 5.500,
            -121.158, -0.254, -28.333,
            -89.180, -29.814, -28.487,
            -121.158, -0.254, -28.333,
            -121.158, -0.180, 5.500,
            -89.180, 29.878, 5.211,
            -89.180, -30.239, 5.344,
            -89.180, -29.814, -28.487,
            -57.202, -0.255, -28.333,
            -89.180, -29.814, -28.487,
            -89.180, -30.239, 5.344,
            -121.158, -0.180, 5.500,
            -89.180, 29.878, 5.211,
            -89.180, 29.304, -28.617,
            -121.158, -0.254, -28.333,
            -89.180, 29.304, -28.617,
            -89.180, 29.878, 5.211,
            -57.202, -0.180, 5.500,
            -115.649, -0.083, 49.156,
            -115.649, -0.063, 58.553,
            -89.180, 25.466, 58.340,
            -115.649, -0.063, 58.553,
            -115.649, -0.083, 49.156,
            -89.180, -25.498, 49.057,
            -62.711, -0.063, 58.553,
            -62.711, -0.083, 49.156,
            -89.180, 25.331, 48.945,
            -62.711, -0.083, 49.156,
            -62.711, -0.063, 58.553,
            -89.180, -25.592, 58.453,
            -89.180, -25.498, 49.057,
            -89.180, -25.592, 58.453,
            -115.649, -0.063, 58.553,
            -89.180, -25.592, 58.453,
            -89.180, -25.498, 49.057,
            -62.711, -0.083, 49.156,
            -89.180, 25.466, 58.340,
            -89.180, 25.331, 48.945,
            -115.649, -0.083, 49.156,
            -89.180, 25.331, 48.945,
            -89.180, 25.466, 58.340,
            -62.711, -0.063, 58.553,
            89.180, -31.736, -3.124,
            89.180, -32.477, 52.347,
            55.490, -0.076, 52.527,
            89.180, 31.338, -3.264,
            89.180, 32.324, 52.204,
            122.871, -0.076, 52.527,
            122.871, -0.076, 52.527,
            122.871, -0.198, -2.949,
            89.180, 31.338, -3.264,
            55.490, -0.076, 52.527,
            55.490, -0.198, -2.949,
            89.180, -31.736, -3.124,
            89.180, 35.078, -27.775,
            127.385, -0.253, -27.385,
            89.180, -35.584, -27.619,
            50.975, -0.206, -6.469,
            89.180, -35.907, -6.705,
            127.385, -0.206, -6.469,
            89.180, -35.584, -27.619,
            50.975, -0.253, -27.385,
            89.180, 35.078, -27.775,
            127.385, -0.206, -6.469,
            89.180, 35.493, -6.863,
            50.975, -0.206, -6.469,
            89.180, 19.289, 57.144,
            109.260, -0.065, 57.277,
            89.180, -19.421, 57.230,
            109.260, -0.057, 61.207,
            89.180, 19.334, 61.075,
            69.100, -0.057, 61.207,
            89.180, -19.421, 57.230,
            69.100, -0.066, 57.277,
            89.180, 19.289, 57.144,
            69.100, -0.057, 61.207,
            89.180, -19.448, 61.160,
            109.260, -0.057, 61.207,
            55.490, -0.198, -2.949,
            55.490, -0.076, 52.527,
            89.180, 32.324, 52.204,
            122.871, -0.076, 52.527,
            89.180, 32.324, 52.204,
            55.490, -0.076, 52.527,
            122.871, -0.198, -2.949,
            122.871, -0.076, 52.527,
            89.180, -32.477, 52.347,
            55.490, -0.076, 52.527,
            89.180, -32.477, 52.347,
            122.871, -0.076, 52.527,
            89.180, 31.338, -3.264,
            122.871, -0.198, -2.949,
            89.180, -31.736, -3.124,
            89.180, -32.477, 52.347,
            89.180, -31.736, -3.124,
            122.871, -0.198, -2.949,
            89.180, 32.324, 52.204,
            89.180, 31.338, -3.264,
            55.490, -0.198, -2.949,
            89.180, -31.736, -3.124,
            55.490, -0.198, -2.949,
            89.180, 31.338, -3.264,
            127.385, -0.253, -27.385,
            127.385, -0.206, -6.469,
            89.180, -35.907, -6.705,
            127.385, -0.206, -6.469,
            127.385, -0.253, -27.385,
            89.180, 35.078, -27.775,
            50.975, -0.206, -6.469,
            50.975, -0.253, -27.385,
            89.180, -35.584, -27.619,
            50.975, -0.253, -27.385,
            50.975, -0.206, -6.469,
            89.180, 35.493, -6.863,
            89.180, -35.907, -6.705,
            89.180, -35.584, -27.619,
            127.385, -0.253, -27.385,
            89.180, -35.584, -27.619,
            89.180, -35.907, -6.705,
            50.975, -0.206, -6.469,
            89.180, 35.493, -6.863,
            89.180, 35.078, -27.775,
            50.975, -0.253, -27.385,
            89.180, 35.078, -27.775,
            89.180, 35.493, -6.863,
            127.385, -0.206, -6.469,
            69.100, -0.066, 57.277,
            69.100, -0.057, 61.207,
            89.180, 19.334, 61.075,
            69.100, -0.057, 61.207,
            69.100, -0.066, 57.277,
            89.180, -19.421, 57.230,
            109.260, -0.057, 61.207,
            109.260, -0.065, 57.277,
            89.180, 19.289, 57.144,
            109.260, -0.065, 57.277,
            109.260, -0.057, 61.207,
            89.180, -19.448, 61.160,
            89.180, 19.334, 61.075,
            89.180, 19.289, 57.144,
            69.100, -0.066, 57.277,
            89.180, 19.289, 57.144,
            89.180, 19.334, 61.075,
            109.260, -0.057, 61.207,
            89.180, -19.421, 57.230,
            89.180, -19.448, 61.160,
            69.100, -0.057, 61.207,
            89.180, -19.448, 61.160,
            89.180, -19.421, 57.230,
            109.260, -0.065, 57.277,
            267.540, -35.611, -25.892,
            267.540, -36.564, 35.872,
            229.335, -0.112, 36.114,
            267.540, 35.113, -26.049,
            267.540, 36.339, 35.712,
            305.745, -0.112, 36.114,
            229.335, -0.112, 36.114,
            229.335, -0.249, -25.659,
            267.540, -35.611, -25.892,
            305.745, -0.112, 36.114,
            305.745, -0.248, -25.659,
            267.540, 35.113, -26.049,
            267.540, -32.309, 39.817,
            233.850, -0.104, 39.997,
            267.540, 32.101, 39.675,
            301.231, -0.089, 46.660,
            267.540, 32.219, 46.337,
            233.850, -0.089, 46.660,
            267.540, 32.101, 39.675,
            301.231, -0.104, 39.997,
            267.540, -32.309, 39.817,
            233.850, -0.089, 46.660,
            267.540, -32.398, 46.480,
            301.231, -0.089, 46.660,
            267.540, 24.934, 52.112,
            293.549, -0.076, 52.318,
            267.540, -25.087, 52.222,
            293.549, -0.065, 57.461,
            267.540, 25.007, 57.255,
            241.531, -0.065, 57.461,
            267.540, -25.087, 52.222,
            241.531, -0.076, 52.317,
            267.540, 24.934, 52.112,
            241.531, -0.065, 57.461,
            267.540, -25.138, 57.366,
            293.549, -0.065, 57.461,
            229.335, -0.249, -25.659,
            229.335, -0.112, 36.114,
            267.540, 36.339, 35.712,
            305.745, -0.112, 36.114,
            267.540, 36.339, 35.712,
            229.335, -0.112, 36.114,
            305.745, -0.248, -25.659,
            305.745, -0.112, 36.114,
            267.540, -36.564, 35.872,
            229.335, -0.112, 36.114,
            267.540, -36.564, 35.872,
            305.745, -0.112, 36.114,
            267.540, 36.339, 35.712,
            267.540, 35.113, -26.049,
            229.335, -0.249, -25.659,
            267.540, -35.611, -25.892,
            229.335, -0.249, -25.659,
            267.540, 35.113, -26.049,
            267.540, -36.564, 35.872,
            267.540, -35.611, -25.892,
            305.745, -0.248, -25.659,
            267.540, 35.113, -26.049,
            305.745, -0.248, -25.659,
            267.540, -35.611, -25.892,
            241.531, -0.076, 52.317,
            241.531, -0.065, 57.461,
            267.540, 25.007, 57.255,
            241.531, -0.065, 57.461,
            241.531, -0.076, 52.317,
            267.540, -25.087, 52.222,
            293.549, -0.065, 57.461,
            293.549, -0.076, 52.318,
            267.540, 24.934, 52.112,
            293.549, -0.076, 52.318,
            293.549, -0.065, 57.461,
            267.540, -25.138, 57.366,
            267.540, 25.007, 57.255,
            267.540, 24.934, 52.112,
            241.531, -0.076, 52.317,
            267.540, 24.934, 52.112,
            267.540, 25.007, 57.255,
            293.549, -0.065, 57.461,
            267.540, -25.087, 52.222,
            267.540, -25.138, 57.366,
            241.531, -0.065, 57.461,
            267.540, -25.138, 57.366,
            267.540, -25.087, 52.222,
            293.549, -0.076, 52.318,
            301.231, -0.104, 39.997,
            301.231, -0.089, 46.660,
            267.540, -32.398, 46.480,
            301.231, -0.089, 46.660,
            301.231, -0.104, 39.997,
            267.540, 32.101, 39.675,
            233.850, -0.104, 39.997,
            233.850, -0.089, 46.660,
            267.540, 32.219, 46.337,
            233.850, -0.089, 46.660,
            233.850, -0.104, 39.997,
            267.540, -32.309, 39.817,
            267.540, 32.219, 46.337,
            267.540, 32.101, 39.675,
            233.850, -0.104, 39.997,
            267.540, 32.101, 39.675,
            267.540, 32.219, 46.337,
            301.231, -0.089, 46.660,
            267.540, -32.309, 39.817,
            267.540, -32.398, 46.480,
            233.850, -0.089, 46.660,
            267.540, -32.398, 46.480,
            267.540, -32.309, 39.817,
               301.231, -0.104, 39.997
            ])
         },
         normal: {
            itemSize: 3,
            array: new Float32Array([
            0.694, -0.720, 0.002,
            0.694, -0.720, 0.002,
            0.694, -0.720, 0.002,
            0.694, -0.720, -0.010,
            0.694, -0.720, -0.010,
            0.694, -0.720, -0.010,
            0.694, 0.719, -0.013,
            0.694, 0.719, -0.013,
            0.694, 0.719, -0.013,
            0.694, 0.720, -0.002,
            0.694, 0.720, -0.002,
            0.694, 0.720, -0.002,
            -0.694, 0.720, -0.002,
            -0.694, 0.720, -0.002,
            -0.694, 0.720, -0.002,
            -0.694, 0.720, -0.013,
            -0.694, 0.720, -0.013,
            -0.694, 0.720, -0.013,
            -0.694, -0.720, -0.010,
            -0.694, -0.720, -0.010,
            -0.694, -0.720, -0.010,
            -0.694, -0.720, 0.002,
            -0.694, -0.720, 0.002,
            -0.694, -0.720, 0.002,
            -0.000, 0.010, 1.000,
            -0.000, 0.010, 1.000,
            -0.000, 0.010, 1.000,
            0.000, -0.006, 1.000,
            0.000, -0.006, 1.000,
            0.000, -0.006, 1.000,
            0.007, -0.002, -1.000,
            0.007, -0.002, -1.000,
            0.007, -0.002, -1.000,
            -0.007, -0.002, -1.000,
            -0.007, -0.002, -1.000,
            -0.007, -0.002, -1.000,
            0.692, -0.721, 0.002,
            0.692, -0.721, 0.002,
            0.692, -0.721, 0.002,
            0.689, -0.725, -0.013,
            0.689, -0.725, -0.013,
            0.689, -0.725, -0.013,
            0.692, 0.721, -0.017,
            0.692, 0.721, -0.017,
            0.692, 0.721, -0.017,
            0.689, 0.725, -0.002,
            0.689, 0.725, -0.002,
            0.689, 0.725, -0.002,
            -0.692, 0.721, -0.002,
            -0.692, 0.721, -0.002,
            -0.692, 0.721, -0.002,
            -0.689, 0.725, -0.017,
            -0.689, 0.725, -0.017,
            -0.689, 0.725, -0.017,
            -0.692, -0.721, -0.013,
            -0.692, -0.721, -0.013,
            -0.692, -0.721, -0.013,
            -0.689, -0.725, 0.002,
            -0.689, -0.725, 0.002,
            -0.689, -0.725, 0.002,
            0.000, 0.013, 1.000,
            0.000, 0.013, 1.000,
            -0.000, 0.013, 1.000,
            0.000, -0.008, 1.000,
            0.000, -0.008, 1.000,
            0.000, -0.008, 1.000,
            0.010, -0.002, -1.000,
            0.010, -0.002, -1.000,
            0.010, -0.002, -1.000,
            -0.010, -0.002, -1.000,
            -0.010, -0.002, -1.000,
            -0.010, -0.002, -1.000,
            0.688, -0.725, 0.002,
            0.688, -0.725, 0.002,
            0.688, -0.725, 0.002,
            0.679, -0.734, -0.011,
            0.679, -0.734, -0.011,
            0.679, -0.734, -0.011,
            0.688, 0.725, -0.014,
            0.688, 0.725, -0.014,
            0.688, 0.725, -0.014,
            0.679, 0.734, -0.002,
            0.679, 0.734, -0.002,
            0.679, 0.734, -0.002,
            -0.688, 0.725, -0.002,
            -0.688, 0.725, -0.002,
            -0.688, 0.725, -0.002,
            -0.679, 0.734, -0.015,
            -0.679, 0.734, -0.015,
            -0.679, 0.734, -0.015,
            -0.688, -0.725, -0.011,
            -0.688, -0.725, -0.011,
            -0.688, -0.725, -0.011,
            -0.679, -0.734, 0.002,
            -0.679, -0.734, 0.002,
            -0.679, -0.734, 0.002,
            0.000, 0.011, 1.000,
            0.000, 0.011, 1.000,
            -0.000, 0.011, 1.000,
            0.000, -0.007, 1.000,
            0.000, -0.007, 1.000,
            0.000, -0.007, 1.000,
            0.008, -0.002, -1.000,
            0.008, -0.002, -1.000,
            0.008, -0.002, -1.000,
            -0.008, -0.002, -1.000,
            -0.008, -0.002, -1.000,
            -0.008, -0.002, -1.000,
            -0.008, -0.002, -1.000,
            -0.008, -0.002, -1.000,
            -0.008, -0.002, -1.000,
            0.000, 0.011, 1.000,
            0.000, 0.011, 1.000,
            -0.000, 0.011, 1.000,
            0.008, -0.002, -1.000,
            0.008, -0.002, -1.000,
            0.008, -0.002, -1.000,
            0.000, -0.007, 1.000,
            0.000, -0.007, 1.000,
            0.000, -0.007, 1.000,
            -0.007, -0.002, -1.000,
            -0.007, -0.002, -1.000,
            -0.007, -0.002, -1.000,
            -0.000, 0.010, 1.000,
            -0.000, 0.010, 1.000,
            -0.000, 0.010, 1.000,
            0.007, -0.002, -1.000,
            0.007, -0.002, -1.000,
            0.007, -0.002, -1.000,
            0.000, -0.005, 1.000,
            0.000, -0.005, 1.000,
            0.000, -0.005, 1.000,
            0.006, -0.002, -1.000,
            0.006, -0.002, -1.000,
            0.006, -0.002, -1.000,
            0.000, 0.008, 1.000,
            0.000, 0.008, 1.000,
            -0.000, 0.008, 1.000,
            -0.006, -0.002, -1.000,
            -0.006, -0.002, -1.000,
            -0.006, -0.002, -1.000,
            0.000, -0.004, 1.000,
            0.000, -0.004, 1.000,
            0.000, -0.004, 1.000,
            0.692, -0.722, 0.002,
            0.692, -0.722, 0.002,
            0.692, -0.722, 0.002,
            0.686, 0.728, -0.002,
            0.686, 0.728, -0.002,
            0.686, 0.728, -0.002,
            -0.692, 0.722, -0.002,
            -0.692, 0.722, -0.002,
            -0.692, 0.722, -0.002,
            -0.686, -0.728, 0.002,
            -0.686, -0.728, 0.002,
            -0.686, -0.728, 0.002,
            -0.686, 0.728, -0.014,
            -0.686, 0.728, -0.014,
            -0.686, 0.728, -0.014,
            0.692, 0.722, -0.014,
            0.692, 0.722, -0.014,
            0.692, 0.722, -0.014,
            -0.692, -0.722, -0.011,
            -0.692, -0.722, -0.011,
            -0.692, -0.722, -0.011,
            0.686, -0.728, -0.011,
            0.686, -0.728, -0.011,
            0.686, -0.728, -0.011,
            0.685, -0.729, 0.002,
            0.685, -0.729, 0.002,
            0.685, -0.729, 0.002,
            0.679, 0.734, -0.002,
            0.679, 0.734, -0.002,
            0.679, 0.734, -0.002,
            -0.679, -0.734, 0.002,
            -0.679, -0.734, 0.002,
            -0.679, -0.734, 0.002,
            -0.685, 0.729, -0.002,
            -0.685, 0.729, -0.002,
            -0.685, 0.729, -0.002,
            0.679, -0.734, -0.009,
            0.679, -0.734, -0.009,
            0.679, -0.734, -0.009,
            -0.685, -0.729, -0.009,
            -0.685, -0.729, -0.009,
            -0.685, -0.729, -0.009,
            -0.679, 0.734, -0.012,
            -0.679, 0.734, -0.012,
            -0.679, 0.734, -0.012,
            0.685, 0.729, -0.012,
            0.685, 0.729, -0.012,
            0.685, 0.729, -0.012,
            -0.694, 0.720, -0.002,
            -0.694, 0.720, -0.002,
            -0.694, 0.720, -0.002,
            -0.693, -0.721, 0.002,
            -0.693, -0.721, 0.002,
            -0.693, -0.721, 0.002,
            0.693, 0.721, -0.002,
            0.693, 0.721, -0.002,
            0.693, 0.721, -0.002,
            0.694, -0.720, 0.002,
            0.694, -0.720, 0.002,
            0.694, -0.720, 0.002,
            -0.694, -0.720, -0.007,
            -0.694, -0.720, -0.007,
            -0.694, -0.720, -0.007,
            0.693, -0.721, -0.007,
            0.693, -0.721, -0.007,
            0.693, -0.721, -0.007,
            -0.693, 0.721, -0.010,
            -0.693, 0.721, -0.010,
            -0.693, 0.721, -0.010,
            0.694, 0.720, -0.010,
            0.694, 0.720, -0.010,
            0.694, 0.720, -0.010,
            -0.693, -0.721, -0.010,
            -0.693, -0.721, -0.010,
            -0.693, -0.721, -0.010,
            0.693, 0.721, -0.013,
            0.693, 0.721, -0.013,
            0.693, 0.721, -0.013,
            0.683, 0.730, -0.002,
            0.683, 0.730, -0.002,
            0.683, 0.730, -0.002,
            -0.683, -0.730, 0.002,
            -0.683, -0.730, 0.002,
            -0.683, -0.730, 0.002,
            0.008, -0.002, -1.000,
            0.008, -0.002, -1.000,
            0.008, -0.002, -1.000,
            -0.000, -0.007, 1.000,
            -0.000, -0.007, 1.000,
            -0.000, -0.007, 1.000,
            -0.008, -0.002, -1.000,
            -0.008, -0.002, -1.000,
            -0.008, -0.002, -1.000,
            0.000, 0.011, 1.000,
            0.000, 0.011, 1.000,
            0.000, 0.011, 1.000,
            0.004, -0.002, -1.000,
            0.004, -0.002, -1.000,
            0.004, -0.002, -1.000,
            0.000, 0.007, 1.000,
            0.000, 0.007, 1.000,
            0.000, 0.007, 1.000,
            -0.004, -0.002, -1.000,
            -0.004, -0.002, -1.000,
            -0.004, -0.002, -1.000,
            -0.000, -0.002, 1.000,
            -0.000, -0.002, 1.000,
            -0.000, -0.002, 1.000,
            -0.693, 0.721, -0.002,
            -0.693, 0.721, -0.002,
            -0.693, 0.721, -0.002,
            -0.000, 0.010, 1.000,
            -0.000, 0.010, 1.000,
            -0.000, 0.010, 1.000,
            0.693, -0.721, 0.002,
            0.693, -0.721, 0.002,
            0.693, -0.721, 0.002,
            0.000, -0.006, 1.000,
            0.000, -0.006, 1.000,
            0.000, -0.006, 1.000,
            0.007, -0.002, -1.000,
            0.007, -0.002, -1.000,
            0.007, -0.002, -1.000,
            0.683, -0.730, -0.010,
            0.683, -0.730, -0.010,
            0.683, -0.730, -0.010,
            -0.683, 0.730, -0.013,
            -0.683, 0.730, -0.013,
            -0.683, 0.730, -0.013,
            -0.007, -0.002, -1.000,
            -0.007, -0.002, -1.000,
            -0.007, -0.002, -1.000,
            0.683, -0.731, 0.002,
            0.683, -0.731, 0.002,
            0.683, -0.731, 0.002,
            0.679, 0.734, -0.002,
            0.679, 0.734, -0.002,
            0.679, 0.734, -0.002,
            -0.679, -0.734, 0.002,
            -0.679, -0.734, 0.002,
            -0.679, -0.734, 0.002,
            -0.683, 0.731, -0.002,
            -0.683, 0.731, -0.002,
            -0.683, 0.731, -0.002,
            0.679, -0.734, -0.011,
            0.679, -0.734, -0.011,
            0.679, -0.734, -0.011,
            -0.683, -0.731, -0.011,
            -0.683, -0.731, -0.011,
            -0.683, -0.731, -0.011,
            -0.679, 0.734, -0.015,
            -0.679, 0.734, -0.015,
            -0.679, 0.734, -0.015,
            0.683, 0.731, -0.015,
            0.683, 0.731, -0.015,
            0.683, 0.731, -0.015,
            -0.695, 0.719, -0.002,
            -0.695, 0.719, -0.002,
            -0.695, 0.719, -0.002,
            -0.694, -0.720, 0.002,
            -0.694, -0.720, 0.002,
            -0.694, -0.720, 0.002,
            0.694, 0.720, -0.002,
            0.694, 0.720, -0.002,
            0.694, 0.720, -0.002,
            0.695, -0.719, 0.002,
            0.695, -0.719, 0.002,
            0.695, -0.719, 0.002,
            -0.694, 0.720, -0.008,
            -0.694, 0.720, -0.008,
            -0.694, 0.720, -0.008,
            0.695, 0.719, -0.008,
            0.695, 0.719, -0.008,
            0.695, 0.719, -0.008,
            -0.695, -0.719, -0.005,
            -0.695, -0.719, -0.005,
            -0.695, -0.719, -0.005,
            0.694, -0.720, -0.005,
            0.694, -0.720, -0.005,
            0.694, -0.720, -0.005,
            -0.690, -0.723, -0.011,
            -0.690, -0.723, -0.011,
            -0.690, -0.723, -0.011,
            0.690, 0.723, -0.014,
            0.690, 0.723, -0.014,
            0.690, 0.723, -0.014,
            -0.679, -0.734, 0.002,
            -0.679, -0.734, 0.002,
            -0.679, -0.734, 0.002,
            0.679, 0.734, -0.002,
            0.679, 0.734, -0.002,
            0.679, 0.734, -0.002,
            -0.007, -0.002, -1.000,
            -0.007, -0.002, -1.000,
            -0.007, -0.002, -1.000,
            -0.000, 0.010, 1.000,
            -0.000, 0.010, 1.000,
            -0.000, 0.010, 1.000,
            0.007, -0.002, -1.000,
            0.007, -0.002, -1.000,
            0.007, -0.002, -1.000,
            0.000, -0.006, 1.000,
            0.000, -0.006, 1.000,
            0.000, -0.006, 1.000,
            0.006, -0.002, -1.000,
            0.006, -0.002, -1.000,
            0.006, -0.002, -1.000,
            0.000, 0.008, 1.000,
            0.000, 0.008, 1.000,
            0.000, 0.008, 1.000,
            -0.006, -0.002, -1.000,
            -0.006, -0.002, -1.000,
            -0.006, -0.002, -1.000,
            0.000, -0.004, 1.000,
            0.000, -0.004, 1.000,
            0.000, -0.004, 1.000,
            -0.690, 0.724, -0.002,
            -0.690, 0.724, -0.002,
            -0.690, 0.724, -0.002,
            -0.000, 0.011, 1.000,
            -0.000, 0.011, 1.000,
            -0.000, 0.011, 1.000,
            0.690, -0.724, 0.002,
            0.690, -0.724, 0.002,
            0.690, -0.724, 0.002,
            0.000, -0.007, 1.000,
            0.000, -0.007, 1.000,
            0.000, -0.007, 1.000,
            -0.679, 0.734, -0.015,
            -0.679, 0.734, -0.015,
            -0.679, 0.734, -0.015,
            -0.008, -0.002, -1.000,
            -0.008, -0.002, -1.000,
            -0.008, -0.002, -1.000,
            0.679, -0.734, -0.011,
            0.679, -0.734, -0.011,
            0.679, -0.734, -0.011,
            0.008, -0.002, -1.000,
            0.008, -0.002, -1.000,
            0.008, -0.002, -1.000,
            -0.694, 0.720, -0.002,
            -0.694, 0.720, -0.002,
            -0.694, 0.720, -0.002,
            -0.693, -0.721, 0.002,
            -0.693, -0.721, 0.002,
            -0.693, -0.721, 0.002,
            0.693, 0.721, -0.002,
            0.693, 0.721, -0.002,
            0.693, 0.721, -0.002,
            0.694, -0.720, 0.002,
            0.694, -0.720, 0.002,
            0.694, -0.720, 0.002,
            -0.693, 0.721, -0.010,
            -0.693, 0.721, -0.010,
            -0.693, 0.721, -0.010,
            0.694, 0.720, -0.010,
            0.694, 0.720, -0.010,
            0.694, 0.720, -0.010,
            -0.694, -0.720, -0.007,
            -0.694, -0.720, -0.007,
            -0.694, -0.720, -0.007,
            0.693, -0.721, -0.007,
            0.693, -0.721, -0.007,
            0.693, -0.721, -0.007,
            0.692, -0.722, 0.002,
            0.692, -0.722, 0.002,
            0.692, -0.722, 0.002,
            0.691, 0.723, -0.002,
            0.691, 0.723, -0.002,
            0.691, 0.723, -0.002,
            -0.692, 0.722, -0.002,
            -0.692, 0.722, -0.002,
            -0.692, 0.722, -0.002,
            -0.691, -0.723, 0.002,
            -0.691, -0.723, 0.002,
            -0.691, -0.723, 0.002,
            -0.691, 0.723, -0.013,
            -0.691, 0.723, -0.013,
            -0.691, 0.723, -0.013,
            0.692, 0.722, -0.013,
            0.692, 0.722, -0.013,
            0.692, 0.722, -0.013,
            -0.692, -0.722, -0.010,
            -0.692, -0.722, -0.010,
            -0.692, -0.722, -0.010,
            0.691, -0.723, -0.010,
            0.691, -0.723, -0.010,
               0.691, -0.723, -0.010
            ])
         }
         };

         material = new THREE.MeshPhongMaterial({
            color: 0xE6E8E8,
            shininess: 50.000,
            ambient: 0x000000,
            side: THREE.FrontSide,
            specular: 0x333333
            });
         mesh = new THREE.Mesh(geometry, material);
         groupModel4.add(mesh);

}
