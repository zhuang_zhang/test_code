function AddFiveModel16(scene)
{
      var mesh;
      var material;
      var geometry;
         geometry = new THREE.BufferGeometry();
         geometry.attributes = {
         position: {
            itemSize: 3,
            array: new Float32Array([
            -305.414, -177.741, -171.896,
            -305.414, 176.142, -171.896,
            296.668, 176.142, -171.896,
            296.668, 176.142, -171.896,
            296.668, -177.741, -171.896,
            -305.414, -177.741, -171.896,
            -305.414, -177.741, -161.896,
            296.668, -177.741, -161.896,
            296.668, 176.142, -161.896,
            296.668, 176.142, -161.896,
            -305.414, 176.142, -161.896,
            -305.414, -177.741, -161.896,
            -305.414, -177.741, -171.896,
            296.668, -177.741, -171.896,
            296.668, -177.741, -161.896,
            296.668, -177.741, -161.896,
            -305.414, -177.741, -161.896,
            -305.414, -177.741, -171.896,
            296.668, -177.741, -171.896,
            296.668, 176.142, -171.896,
            296.668, 176.142, -161.896,
            296.668, 176.142, -161.896,
            296.668, -177.741, -161.896,
            296.668, -177.741, -171.896,
            296.668, 176.142, -171.896,
            -305.414, 176.142, -171.896,
            -305.414, 176.142, -161.896,
            -305.414, 176.142, -161.896,
            296.668, 176.142, -161.896,
            296.668, 176.142, -171.896,
            -305.414, 176.142, -171.896,
            -305.414, -177.741, -171.896,
            -305.414, -177.741, -161.896,
            -305.414, -177.741, -161.896,
            -305.414, 176.142, -161.896,
               -305.414, 176.142, -171.896
            ])
         },
         normal: {
            itemSize: 3,
            array: new Float32Array([
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
               -1.000, 0.000, 0.000
            ])
         }
         };

         material = new THREE.MeshPhongMaterial({
            color: 0x969696,
            shininess: 2.000,
            ambient: 0x969696,
            side: THREE.FrontSide,
            specular: 0xE5E5E5
            });
         mesh = new THREE.Mesh(geometry, material);
         groupModel5.add(mesh);

}
