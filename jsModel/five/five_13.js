function AddFiveModel13(scene)
{
      var mesh;
      var material;
      var geometry;
         geometry = new THREE.BufferGeometry();
         geometry.attributes = {
         position: {
            itemSize: 3,
            array: new Float32Array([
            485.702, -157.469, -249.518,
            485.702, -147.469, -249.518,
            535.702, -147.469, -249.518,
            535.702, -147.469, -249.518,
            535.702, -157.469, -249.518,
            485.702, -157.469, -249.518,
            485.702, -157.469, -209.518,
            535.702, -157.469, -209.518,
            535.702, -147.469, -209.518,
            535.702, -147.469, -209.518,
            485.702, -147.469, -209.518,
            485.702, -157.469, -209.518,
            485.702, -157.469, -249.518,
            535.702, -157.469, -249.518,
            535.702, -157.469, -209.518,
            535.702, -157.469, -209.518,
            485.702, -157.469, -209.518,
            485.702, -157.469, -249.518,
            535.702, -157.469, -249.518,
            535.702, -147.469, -249.518,
            535.702, -147.469, -209.518,
            535.702, -147.469, -209.518,
            535.702, -157.469, -209.518,
            535.702, -157.469, -249.518,
            535.702, -147.469, -249.518,
            485.702, -147.469, -249.518,
            485.702, -147.469, -209.518,
            485.702, -147.469, -209.518,
            535.702, -147.469, -209.518,
            535.702, -147.469, -249.518,
            485.702, -147.469, -249.518,
            485.702, -157.469, -249.518,
            485.702, -157.469, -209.518,
            485.702, -157.469, -209.518,
            485.702, -147.469, -209.518,
            485.702, -147.469, -249.518,
            665.863, -157.469, -249.518,
            665.863, -147.469, -249.518,
            715.863, -147.469, -249.518,
            715.863, -147.469, -249.518,
            715.863, -157.469, -249.518,
            665.863, -157.469, -249.518,
            665.863, -157.469, -209.518,
            715.863, -157.469, -209.518,
            715.863, -147.469, -209.518,
            715.863, -147.469, -209.518,
            665.863, -147.469, -209.518,
            665.863, -157.469, -209.518,
            665.863, -157.469, -249.518,
            715.863, -157.469, -249.518,
            715.863, -157.469, -209.518,
            715.863, -157.469, -209.518,
            665.863, -157.469, -209.518,
            665.863, -157.469, -249.518,
            715.863, -157.469, -249.518,
            715.863, -147.469, -249.518,
            715.863, -147.469, -209.518,
            715.863, -147.469, -209.518,
            715.863, -157.469, -209.518,
            715.863, -157.469, -249.518,
            715.863, -147.469, -249.518,
            665.863, -147.469, -249.518,
            665.863, -147.469, -209.518,
            665.863, -147.469, -209.518,
            715.863, -147.469, -209.518,
            715.863, -147.469, -249.518,
            665.863, -147.469, -249.518,
            665.863, -157.469, -249.518,
            665.863, -157.469, -209.518,
            665.863, -157.469, -209.518,
            665.863, -147.469, -209.518,
            665.863, -147.469, -249.518,
            854.466, -157.469, -249.518,
            854.466, -147.469, -249.518,
            904.466, -147.469, -249.518,
            904.466, -147.469, -249.518,
            904.466, -157.469, -249.518,
            854.466, -157.469, -249.518,
            854.466, -157.469, -209.518,
            904.466, -157.469, -209.518,
            904.466, -147.469, -209.518,
            904.466, -147.469, -209.518,
            854.466, -147.469, -209.518,
            854.466, -157.469, -209.518,
            854.466, -157.469, -249.518,
            904.466, -157.469, -249.518,
            904.466, -157.469, -209.518,
            904.466, -157.469, -209.518,
            854.466, -157.469, -209.518,
            854.466, -157.469, -249.518,
            904.466, -157.469, -249.518,
            904.466, -147.469, -249.518,
            904.466, -147.469, -209.518,
            904.466, -147.469, -209.518,
            904.466, -157.469, -209.518,
            904.466, -157.469, -249.518,
            904.466, -147.469, -249.518,
            854.466, -147.469, -249.518,
            854.466, -147.469, -209.518,
            854.466, -147.469, -209.518,
            904.466, -147.469, -209.518,
            904.466, -147.469, -249.518,
            854.466, -147.469, -249.518,
            854.466, -157.469, -249.518,
            854.466, -157.469, -209.518,
            854.466, -157.469, -209.518,
            854.466, -147.469, -209.518,
            854.466, -147.469, -249.518,
            -910.871, -157.469, -244.350,
            -910.871, -147.469, -244.350,
            -860.871, -147.469, -244.350,
            -860.871, -147.469, -244.350,
            -860.871, -157.469, -244.350,
            -910.871, -157.469, -244.350,
            -910.871, -157.469, -204.350,
            -860.871, -157.469, -204.350,
            -860.871, -147.469, -204.350,
            -860.871, -147.469, -204.350,
            -910.871, -147.469, -204.350,
            -910.871, -157.469, -204.350,
            -910.871, -157.469, -244.350,
            -860.871, -157.469, -244.350,
            -860.871, -157.469, -204.350,
            -860.871, -157.469, -204.350,
            -910.871, -157.469, -204.350,
            -910.871, -157.469, -244.350,
            -860.871, -157.469, -244.350,
            -860.871, -147.469, -244.350,
            -860.871, -147.469, -204.350,
            -860.871, -147.469, -204.350,
            -860.871, -157.469, -204.350,
            -860.871, -157.469, -244.350,
            -860.871, -147.469, -244.350,
            -910.871, -147.469, -244.350,
            -910.871, -147.469, -204.350,
            -910.871, -147.469, -204.350,
            -860.871, -147.469, -204.350,
            -860.871, -147.469, -244.350,
            -910.871, -147.469, -244.350,
            -910.871, -157.469, -244.350,
            -910.871, -157.469, -204.350,
            -910.871, -157.469, -204.350,
            -910.871, -147.469, -204.350,
            -910.871, -147.469, -244.350,
            -730.711, -157.469, -244.350,
            -730.711, -147.469, -244.350,
            -680.711, -147.469, -244.350,
            -680.711, -147.469, -244.350,
            -680.711, -157.469, -244.350,
            -730.711, -157.469, -244.350,
            -730.711, -157.469, -204.350,
            -680.711, -157.469, -204.350,
            -680.711, -147.469, -204.350,
            -680.711, -147.469, -204.350,
            -730.711, -147.469, -204.350,
            -730.711, -157.469, -204.350,
            -730.711, -157.469, -244.350,
            -680.711, -157.469, -244.350,
            -680.711, -157.469, -204.350,
            -680.711, -157.469, -204.350,
            -730.711, -157.469, -204.350,
            -730.711, -157.469, -244.350,
            -680.711, -157.469, -244.350,
            -680.711, -147.469, -244.350,
            -680.711, -147.469, -204.350,
            -680.711, -147.469, -204.350,
            -680.711, -157.469, -204.350,
            -680.711, -157.469, -244.350,
            -680.711, -147.469, -244.350,
            -730.711, -147.469, -244.350,
            -730.711, -147.469, -204.350,
            -730.711, -147.469, -204.350,
            -680.711, -147.469, -204.350,
            -680.711, -147.469, -244.350,
            -730.711, -147.469, -244.350,
            -730.711, -157.469, -244.350,
            -730.711, -157.469, -204.350,
            -730.711, -157.469, -204.350,
            -730.711, -147.469, -204.350,
            -730.711, -147.469, -244.350,
            -542.107, -157.469, -244.350,
            -542.107, -147.469, -244.350,
            -492.107, -147.469, -244.350,
            -492.107, -147.469, -244.350,
            -492.107, -157.469, -244.350,
            -542.107, -157.469, -244.350,
            -542.107, -157.469, -204.350,
            -492.107, -157.469, -204.350,
            -492.107, -147.469, -204.350,
            -492.107, -147.469, -204.350,
            -542.107, -147.469, -204.350,
            -542.107, -157.469, -204.350,
            -542.107, -157.469, -244.350,
            -492.107, -157.469, -244.350,
            -492.107, -157.469, -204.350,
            -492.107, -157.469, -204.350,
            -542.107, -157.469, -204.350,
            -542.107, -157.469, -244.350,
            -492.107, -157.469, -244.350,
            -492.107, -147.469, -244.350,
            -492.107, -147.469, -204.350,
            -492.107, -147.469, -204.350,
            -492.107, -157.469, -204.350,
            -492.107, -157.469, -244.350,
            -492.107, -147.469, -244.350,
            -542.107, -147.469, -244.350,
            -542.107, -147.469, -204.350,
            -542.107, -147.469, -204.350,
            -492.107, -147.469, -204.350,
            -492.107, -147.469, -244.350,
            -542.107, -147.469, -244.350,
            -542.107, -157.469, -244.350,
            -542.107, -157.469, -204.350,
            -542.107, -157.469, -204.350,
            -542.107, -147.469, -204.350,
               -542.107, -147.469, -244.350
            ])
         },
         normal: {
            itemSize: 3,
            array: new Float32Array([
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
               -1.000, 0.000, 0.000
            ])
         }
         };

         material = new THREE.MeshPhongMaterial({
            color: 0x4C3516,
            shininess: 2.000,
            ambient: 0x4C3516,
            side: THREE.FrontSide,
            specular: 0xE5E5E5
            });
         mesh = new THREE.Mesh(geometry, material);
         groupModel5.add(mesh);

}
