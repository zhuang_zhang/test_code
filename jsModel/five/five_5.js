function AddFiveModel5(scene)
{
      var mesh;
      var material;
      var geometry;
         geometry = new THREE.BufferGeometry();
         geometry.attributes = {
         position: {
            itemSize: 3,
            array: new Float32Array([
            -753.219, -68.699, -223.989,
            -753.219, 41.301, -223.989,
            -643.219, 41.301, -223.989,
            -643.219, 41.301, -223.989,
            -643.219, -68.699, -223.989,
            -753.219, -68.699, -223.989,
            -753.219, -68.699, -203.989,
            -643.219, -68.699, -203.989,
            -643.219, 41.301, -203.989,
            -643.219, 41.301, -203.989,
            -753.219, 41.301, -203.989,
            -753.219, -68.699, -203.989,
            -753.219, -68.699, -223.989,
            -643.219, -68.699, -223.989,
            -643.219, -68.699, -203.989,
            -643.219, -68.699, -203.989,
            -753.219, -68.699, -203.989,
            -753.219, -68.699, -223.989,
            -643.219, -68.699, -223.989,
            -643.219, 41.301, -223.989,
            -643.219, 41.301, -203.989,
            -643.219, 41.301, -203.989,
            -643.219, -68.699, -203.989,
            -643.219, -68.699, -223.989,
            -643.219, 41.301, -223.989,
            -753.219, 41.301, -223.989,
            -753.219, 41.301, -203.989,
            -753.219, 41.301, -203.989,
            -643.219, 41.301, -203.989,
            -643.219, 41.301, -223.989,
            -753.219, 41.301, -223.989,
            -753.219, -68.699, -223.989,
            -753.219, -68.699, -203.989,
            -753.219, -68.699, -203.989,
            -753.219, 41.301, -203.989,
            -753.219, 41.301, -223.989,
            -753.219, -68.699, 98.066,
            -753.219, 41.301, 98.066,
            -643.219, 41.301, 98.066,
            -643.219, 41.301, 98.066,
            -643.219, -68.699, 98.066,
            -753.219, -68.699, 98.066,
            -753.219, -68.699, 118.066,
            -643.219, -68.699, 118.066,
            -643.219, 41.301, 118.066,
            -643.219, 41.301, 118.066,
            -753.219, 41.301, 118.066,
            -753.219, -68.699, 118.066,
            -753.219, -68.699, 98.066,
            -643.219, -68.699, 98.066,
            -643.219, -68.699, 118.066,
            -643.219, -68.699, 118.066,
            -753.219, -68.699, 118.066,
            -753.219, -68.699, 98.066,
            -643.219, -68.699, 98.066,
            -643.219, 41.301, 98.066,
            -643.219, 41.301, 118.066,
            -643.219, 41.301, 118.066,
            -643.219, -68.699, 118.066,
            -643.219, -68.699, 98.066,
            -643.219, 41.301, 98.066,
            -753.219, 41.301, 98.066,
            -753.219, 41.301, 118.066,
            -753.219, 41.301, 118.066,
            -643.219, 41.301, 118.066,
            -643.219, 41.301, 98.066,
            -753.219, 41.301, 98.066,
            -753.219, -68.699, 98.066,
            -753.219, -68.699, 118.066,
            -753.219, -68.699, 118.066,
            -753.219, 41.301, 118.066,
            -753.219, 41.301, 98.066,
            -753.219, -68.699, 63.066,
            -753.219, 41.301, 63.066,
            -643.219, 41.301, 63.066,
            -643.219, 41.301, 63.066,
            -643.219, -68.699, 63.066,
            -753.219, -68.699, 63.066,
            -753.219, -68.699, 83.066,
            -643.219, -68.699, 83.066,
            -643.219, 41.301, 83.066,
            -643.219, 41.301, 83.066,
            -753.219, 41.301, 83.066,
            -753.219, -68.699, 83.066,
            -753.219, -68.699, 63.066,
            -643.219, -68.699, 63.066,
            -643.219, -68.699, 83.066,
            -643.219, -68.699, 83.066,
            -753.219, -68.699, 83.066,
            -753.219, -68.699, 63.066,
            -643.219, -68.699, 63.066,
            -643.219, 41.301, 63.066,
            -643.219, 41.301, 83.066,
            -643.219, 41.301, 83.066,
            -643.219, -68.699, 83.066,
            -643.219, -68.699, 63.066,
            -643.219, 41.301, 63.066,
            -753.219, 41.301, 63.066,
            -753.219, 41.301, 83.066,
            -753.219, 41.301, 83.066,
            -643.219, 41.301, 83.066,
            -643.219, 41.301, 63.066,
            -753.219, 41.301, 63.066,
            -753.219, -68.699, 63.066,
            -753.219, -68.699, 83.066,
            -753.219, -68.699, 83.066,
            -753.219, 41.301, 83.066,
            -753.219, 41.301, 63.066,
            -753.219, -68.699, 28.066,
            -753.219, 41.301, 28.066,
            -643.219, 41.301, 28.066,
            -643.219, 41.301, 28.066,
            -643.219, -68.699, 28.066,
            -753.219, -68.699, 28.066,
            -753.219, -68.699, 48.066,
            -643.219, -68.699, 48.066,
            -643.219, 41.301, 48.066,
            -643.219, 41.301, 48.066,
            -753.219, 41.301, 48.066,
            -753.219, -68.699, 48.066,
            -753.219, -68.699, 28.066,
            -643.219, -68.699, 28.066,
            -643.219, -68.699, 48.066,
            -643.219, -68.699, 48.066,
            -753.219, -68.699, 48.066,
            -753.219, -68.699, 28.066,
            -643.219, -68.699, 28.066,
            -643.219, 41.301, 28.066,
            -643.219, 41.301, 48.066,
            -643.219, 41.301, 48.066,
            -643.219, -68.699, 48.066,
            -643.219, -68.699, 28.066,
            -643.219, 41.301, 28.066,
            -753.219, 41.301, 28.066,
            -753.219, 41.301, 48.066,
            -753.219, 41.301, 48.066,
            -643.219, 41.301, 48.066,
            -643.219, 41.301, 28.066,
            -753.219, 41.301, 28.066,
            -753.219, -68.699, 28.066,
            -753.219, -68.699, 48.066,
            -753.219, -68.699, 48.066,
            -753.219, 41.301, 48.066,
            -753.219, 41.301, 28.066,
            -753.219, -68.699, -6.934,
            -753.219, 41.301, -6.934,
            -643.219, 41.301, -6.934,
            -643.219, 41.301, -6.934,
            -643.219, -68.699, -6.934,
            -753.219, -68.699, -6.934,
            -753.219, -68.699, 13.066,
            -643.219, -68.699, 13.066,
            -643.219, 41.301, 13.066,
            -643.219, 41.301, 13.066,
            -753.219, 41.301, 13.066,
            -753.219, -68.699, 13.066,
            -753.219, -68.699, -6.934,
            -643.219, -68.699, -6.934,
            -643.219, -68.699, 13.066,
            -643.219, -68.699, 13.066,
            -753.219, -68.699, 13.066,
            -753.219, -68.699, -6.934,
            -643.219, -68.699, -6.934,
            -643.219, 41.301, -6.934,
            -643.219, 41.301, 13.066,
            -643.219, 41.301, 13.066,
            -643.219, -68.699, 13.066,
            -643.219, -68.699, -6.934,
            -643.219, 41.301, -6.934,
            -753.219, 41.301, -6.934,
            -753.219, 41.301, 13.066,
            -753.219, 41.301, 13.066,
            -643.219, 41.301, 13.066,
            -643.219, 41.301, -6.934,
            -753.219, 41.301, -6.934,
            -753.219, -68.699, -6.934,
            -753.219, -68.699, 13.066,
            -753.219, -68.699, 13.066,
            -753.219, 41.301, 13.066,
            -753.219, 41.301, -6.934,
            -753.219, -68.699, -41.934,
            -753.219, 41.301, -41.934,
            -643.219, 41.301, -41.934,
            -643.219, 41.301, -41.934,
            -643.219, -68.699, -41.934,
            -753.219, -68.699, -41.934,
            -753.219, -68.699, -21.934,
            -643.219, -68.699, -21.934,
            -643.219, 41.301, -21.934,
            -643.219, 41.301, -21.934,
            -753.219, 41.301, -21.934,
            -753.219, -68.699, -21.934,
            -753.219, -68.699, -41.934,
            -643.219, -68.699, -41.934,
            -643.219, -68.699, -21.934,
            -643.219, -68.699, -21.934,
            -753.219, -68.699, -21.934,
            -753.219, -68.699, -41.934,
            -643.219, -68.699, -41.934,
            -643.219, 41.301, -41.934,
            -643.219, 41.301, -21.934,
            -643.219, 41.301, -21.934,
            -643.219, -68.699, -21.934,
            -643.219, -68.699, -41.934,
            -643.219, 41.301, -41.934,
            -753.219, 41.301, -41.934,
            -753.219, 41.301, -21.934,
            -753.219, 41.301, -21.934,
            -643.219, 41.301, -21.934,
            -643.219, 41.301, -41.934,
            -753.219, 41.301, -41.934,
            -753.219, -68.699, -41.934,
            -753.219, -68.699, -21.934,
            -753.219, -68.699, -21.934,
            -753.219, 41.301, -21.934,
            -753.219, 41.301, -41.934,
            -753.219, -68.699, -76.934,
            -753.219, 41.301, -76.934,
            -643.219, 41.301, -76.934,
            -643.219, 41.301, -76.934,
            -643.219, -68.699, -76.934,
            -753.219, -68.699, -76.934,
            -753.219, -68.699, -56.934,
            -643.219, -68.699, -56.934,
            -643.219, 41.301, -56.934,
            -643.219, 41.301, -56.934,
            -753.219, 41.301, -56.934,
            -753.219, -68.699, -56.934,
            -753.219, -68.699, -76.934,
            -643.219, -68.699, -76.934,
            -643.219, -68.699, -56.934,
            -643.219, -68.699, -56.934,
            -753.219, -68.699, -56.934,
            -753.219, -68.699, -76.934,
            -643.219, -68.699, -76.934,
            -643.219, 41.301, -76.934,
            -643.219, 41.301, -56.934,
            -643.219, 41.301, -56.934,
            -643.219, -68.699, -56.934,
            -643.219, -68.699, -76.934,
            -643.219, 41.301, -76.934,
            -753.219, 41.301, -76.934,
            -753.219, 41.301, -56.934,
            -753.219, 41.301, -56.934,
            -643.219, 41.301, -56.934,
            -643.219, 41.301, -76.934,
            -753.219, 41.301, -76.934,
            -753.219, -68.699, -76.934,
            -753.219, -68.699, -56.934,
            -753.219, -68.699, -56.934,
            -753.219, 41.301, -56.934,
            -753.219, 41.301, -76.934,
            -753.219, -68.699, -111.934,
            -753.219, 41.301, -111.934,
            -643.219, 41.301, -111.934,
            -643.219, 41.301, -111.934,
            -643.219, -68.699, -111.934,
            -753.219, -68.699, -111.934,
            -753.219, -68.699, -91.934,
            -643.219, -68.699, -91.934,
            -643.219, 41.301, -91.934,
            -643.219, 41.301, -91.934,
            -753.219, 41.301, -91.934,
            -753.219, -68.699, -91.934,
            -753.219, -68.699, -111.934,
            -643.219, -68.699, -111.934,
            -643.219, -68.699, -91.934,
            -643.219, -68.699, -91.934,
            -753.219, -68.699, -91.934,
            -753.219, -68.699, -111.934,
            -643.219, -68.699, -111.934,
            -643.219, 41.301, -111.934,
            -643.219, 41.301, -91.934,
            -643.219, 41.301, -91.934,
            -643.219, -68.699, -91.934,
            -643.219, -68.699, -111.934,
            -643.219, 41.301, -111.934,
            -753.219, 41.301, -111.934,
            -753.219, 41.301, -91.934,
            -753.219, 41.301, -91.934,
            -643.219, 41.301, -91.934,
            -643.219, 41.301, -111.934,
            -753.219, 41.301, -111.934,
            -753.219, -68.699, -111.934,
            -753.219, -68.699, -91.934,
            -753.219, -68.699, -91.934,
            -753.219, 41.301, -91.934,
            -753.219, 41.301, -111.934,
            -753.219, -68.699, -146.934,
            -753.219, 41.301, -146.934,
            -643.219, 41.301, -146.934,
            -643.219, 41.301, -146.934,
            -643.219, -68.699, -146.934,
            -753.219, -68.699, -146.934,
            -753.219, -68.699, -126.934,
            -643.219, -68.699, -126.934,
            -643.219, 41.301, -126.934,
            -643.219, 41.301, -126.934,
            -753.219, 41.301, -126.934,
            -753.219, -68.699, -126.934,
            -753.219, -68.699, -146.934,
            -643.219, -68.699, -146.934,
            -643.219, -68.699, -126.934,
            -643.219, -68.699, -126.934,
            -753.219, -68.699, -126.934,
            -753.219, -68.699, -146.934,
            -643.219, -68.699, -146.934,
            -643.219, 41.301, -146.934,
            -643.219, 41.301, -126.934,
            -643.219, 41.301, -126.934,
            -643.219, -68.699, -126.934,
            -643.219, -68.699, -146.934,
            -643.219, 41.301, -146.934,
            -753.219, 41.301, -146.934,
            -753.219, 41.301, -126.934,
            -753.219, 41.301, -126.934,
            -643.219, 41.301, -126.934,
            -643.219, 41.301, -146.934,
            -753.219, 41.301, -146.934,
            -753.219, -68.699, -146.934,
            -753.219, -68.699, -126.934,
            -753.219, -68.699, -126.934,
            -753.219, 41.301, -126.934,
            -753.219, 41.301, -146.934,
            -753.219, -68.699, -181.934,
            -753.219, 41.301, -181.934,
            -643.219, 41.301, -181.934,
            -643.219, 41.301, -181.934,
            -643.219, -68.699, -181.934,
            -753.219, -68.699, -181.934,
            -753.219, -68.699, -161.934,
            -643.219, -68.699, -161.934,
            -643.219, 41.301, -161.934,
            -643.219, 41.301, -161.934,
            -753.219, 41.301, -161.934,
            -753.219, -68.699, -161.934,
            -753.219, -68.699, -181.934,
            -643.219, -68.699, -181.934,
            -643.219, -68.699, -161.934,
            -643.219, -68.699, -161.934,
            -753.219, -68.699, -161.934,
            -753.219, -68.699, -181.934,
            -643.219, -68.699, -181.934,
            -643.219, 41.301, -181.934,
            -643.219, 41.301, -161.934,
            -643.219, 41.301, -161.934,
            -643.219, -68.699, -161.934,
            -643.219, -68.699, -181.934,
            -643.219, 41.301, -181.934,
            -753.219, 41.301, -181.934,
            -753.219, 41.301, -161.934,
            -753.219, 41.301, -161.934,
            -643.219, 41.301, -161.934,
            -643.219, 41.301, -181.934,
            -753.219, 41.301, -181.934,
            -753.219, -68.699, -181.934,
            -753.219, -68.699, -161.934,
            -753.219, -68.699, -161.934,
            -753.219, 41.301, -161.934,
            -753.219, 41.301, -181.934,
            -753.219, -68.699, -216.934,
            -753.219, 41.301, -216.934,
            -643.219, 41.301, -216.934,
            -643.219, 41.301, -216.934,
            -643.219, -68.699, -216.934,
            -753.219, -68.699, -216.934,
            -753.219, -68.699, -196.934,
            -643.219, -68.699, -196.934,
            -643.219, 41.301, -196.934,
            -643.219, 41.301, -196.934,
            -753.219, 41.301, -196.934,
            -753.219, -68.699, -196.934,
            -753.219, -68.699, -216.934,
            -643.219, -68.699, -216.934,
            -643.219, -68.699, -196.934,
            -643.219, -68.699, -196.934,
            -753.219, -68.699, -196.934,
            -753.219, -68.699, -216.934,
            -643.219, -68.699, -216.934,
            -643.219, 41.301, -216.934,
            -643.219, 41.301, -196.934,
            -643.219, 41.301, -196.934,
            -643.219, -68.699, -196.934,
            -643.219, -68.699, -216.934,
            -643.219, 41.301, -216.934,
            -753.219, 41.301, -216.934,
            -753.219, 41.301, -196.934,
            -753.219, 41.301, -196.934,
            -643.219, 41.301, -196.934,
            -643.219, 41.301, -216.934,
            -753.219, 41.301, -216.934,
            -753.219, -68.699, -216.934,
            -753.219, -68.699, -196.934,
            -753.219, -68.699, -196.934,
            -753.219, 41.301, -196.934,
               -753.219, 41.301, -216.934
            ])
         },
         normal: {
            itemSize: 3,
            array: new Float32Array([
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, -1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, 0.000, 1.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            0.000, -1.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            1.000, 0.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            0.000, 1.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
            -1.000, 0.000, 0.000,
               -1.000, 0.000, 0.000
            ])
         }
         };

         material = new THREE.MeshPhongMaterial({
            color: 0x4580F6,
            shininess: 2.000,
            ambient: 0x4580F6,
            side: THREE.FrontSide,
            specular: 0xE5E5E5
            });
         mesh = new THREE.Mesh(geometry, material);
         groupModel5.add(mesh);

}
