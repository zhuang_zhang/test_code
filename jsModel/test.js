function AddThreeModel0(scene)
{
var geometry = new THREE.BufferGeometry();
var scale = 100;
// create a simple square shape. We duplicate the top left and bottom right
// vertices because each vertex needs to appear once per triangle.
var vertices = new Float32Array( [
            -127.279, -0.000, -113.236,
            -0.000, 127.279, -113.236,
            127.279, 0.000, -113.236,
            127.279, 0.000, -113.236,
            0.000, -127.279, -113.236,
            -127.279, -0.000, -113.236,
            -127.279, -0.000, -24.034,
            0.000, -127.279, -24.034,
            127.279, 0.000, -24.034,
            127.279, 0.000, -24.034,
            -0.000, 127.279, -24.034,
            -127.279, -0.000, -24.034,
            -127.279, -0.000, -113.236,
            0.000, -127.279, -113.236,
            0.000, -127.279, -24.034,
            0.000, -127.279, -24.034,
            -127.279, -0.000, -24.034,
            -127.279, -0.000, -113.236,
            0.000, -127.279, -113.236,
            127.279, 0.000, -113.236,
            127.279, 0.000, -24.034,
            127.279, 0.000, -24.034,
            0.000, -127.279, -24.034,
            0.000, -127.279, -113.236,
            127.279, 0.000, -113.236,
            -0.000, 127.279, -113.236,
            -0.000, 127.279, -24.034,
            -0.000, 127.279, -24.034,
            127.279, 0.000, -24.034,
            127.279, 0.000, -113.236,
            -0.000, 127.279, -113.236,
            -127.279, -0.000, -113.236,
            -127.279, -0.000, -24.034,
            -127.279, -0.000, -24.034,
            -0.000, 127.279, -24.034,
               -0.000, 127.279, -113.236
] );

// itemSize = 3 because there are 3 values (components) per vertex
geometry.addAttribute( 'position', new THREE.BufferAttribute( vertices, 3 ) );
         material = new THREE.MeshPhongMaterial({
            color: 0x4C3516,
            shininess: 15.000,
            ambient: 0x4C3516,
            side: THREE.FrontSide,
            specular: 0xE5E5E5
            });
var mesh = new THREE.Mesh( geometry, material );
console.log(mesh);
scene.add(mesh);
}
